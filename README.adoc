= kuinbound

:toc: left
:toclevels: 4
:icons: font

HTTPInbounds, TCPInbounds, certificates and CA controller.

== Relationships between Kumori and Emissary/Kubernets resources

Note that KuInbound is not a controller: it is a manager of controller (that is: a binary containing several controllers). +
For each primary resource (`kukucert`, `kukuca`, `v3dep`, `kukulink`) an
individual controller exists.

[cols="1,1,2"]
|===
| Primary  | Secondary |  Notes

| `kukucert`
| `secret`
| One-to-one. `secret` will be referenced from `host.tlsSecret`.

| `kukuca`
| `secret`
| One-to-one. `secret` will be referenced from `host.tls.ca_secret`.

| `v3dep` (http inbound)
| `host`
| One to one. `host.hostname` is the related domain (wildcard domain are
allowed, but we dont use it). `host.tlsSecret` is a reference to the `secret`
containing the server certificate. `host.tls.ca_secret.` is a reference to the
`secret` containing a CA (if is used for this host).

| `v3dep` (http inbound)
| `mapping`
| One to one. `mapping.hostname` contains the domain of the related host (so
`host.hostname` = `mapping.hostname`), and this coincidence establishes the relationship between both. `mapping.cluster_tag` is a workaround for a bug
described in https://gitlab.com/kumori/project-management/-/issues/862.

| `v3dep` (http inbound)
| `ingress`
| Consumed by ExternalDNS. New ExternalDNS can work using Emissary resources (adding
specific annotations), so the `ingress` resource could be omitted. But we have
decided to continue using `ingress`, because it is generic.

| `v3dep` (tcp-inbound)
| none
| Only when it is linked some resources will be created

| `v3dep` (special secondaries)
| `kukudomain`, `kukucert`, `kukuca`, `kukulink`, `kukuport`
| This resources are special secondary resources: they are readed to obtain
diverse data and/or triggering the reevaluation, but they are not changed.

| `kukulink` (http inbound)
| `mapping`
| One to many. `mapping.hostname` contains the domain of the related host (so
`host.hostname` = `mapping.hostname`), and this coincidence establishes the relationship between both. The `mapping.precedence=2` gives it greater
precedence over the other mapping. `mapping.cluster_tag` is a workaround for a bug
described in https://gitlab.com/kumori/project-management/-/issues/862.

| `kukulink` (tcp inbound)
| `tcpmapping`
| One to one. `mapping.cluster_tag` is a workaround for a bug
described in https://gitlab.com/kumori/project-management/-/issues/862.

| `kukulink` (special secondaries)
| `kukudomain`, `kukucert`, `kukuca`, `kukuport`, `v3dep`, `host`, `service`
| This resources are special secondary resources: they are readed to obtain
diverse data and/or triggering the reevaluation, but they are not changed.

|===

== Related resources created when a new cluster is launched

Some related resources are created when a new cluster is launched.

=== Listeners

By default, a `listener` in 8443 port (named `emissary-ingress-listener-8443`) is created.

There isnt a listener in 8080 port for HTTP request, but maybe it will be added
in the context of https://gitlab.com/kumori/project-management/-/issues/1083.

No listeners are created for cluster TCP ports (e.g. 9031, 9032,...). Why?
Although it would be plausible, in the case of `tcpmappings` Emissary gives
the facility to delegate to it to create them "on the fly" in the Envoy
configuration (https://www.getambassador.io/docs/emissary/latest/topics/using/tcpmappings#tcpmapping-configuration).

=== Resolvers

A `KubernetesEndpointResolver` named `kumori-default` is created, and it is
referenced from all the created `mappings`. +
It configures Emissary to discover the endpoints of Kubernetes services, and
use specific load balancing configuration.

=== Reference certificate

When a cluster is launched, a `kukucert` resource is created, containing the
TLS certificate for the cluster reference domain (for example: *.test.kumori.cloud),
and named `cluster.core/wildcard-<REFERENCE_DOMAIN>` (for example `cluster.core/wildcard-test-kumori-cloud`).

KuInbound controller, when starting, will process it and create the underlying
secret name `cluster-core-cert-wildcard-<REFERENCE_DOMAIN>-cert` (for example `cluster-core-cert-wildcard-test-deployedin-cloud-cert`)

=== Platform services accessing

For each platform service (Admission, Keycloak, Prometheus, Grafana), a pair of
`host` + `mapping` are created. +
That is: there is no Kumori http-inbound service to be processed by Kuinbound:
the underlying resources are created directly when the cluster is launched.

=== Access to Emissary diagnostic web

An special `host` (without `mapping`) is created to allow access to the Emissary
diagnostic web.

Its name is `kumori-diag`, and its related domain is
`emissary-<CLUSTER_NAME>.<REFERENCE_DOMAIN>`.

By default, diagnostic is not enabled: it can be enabled modifying a `module`
resource named `ambassador` (it doesnt cause Ambassador Pods to restart). +
One diagnostic is enabled, the web is available at
`https://emissary-<CLUSTER_NAME>.<REFERENCE_DOMAIN>/ambassador/v0/diag/`.

== Miscellaneous

=== Configuring subpath routing and mTLS per subpath

These are the cue definitions needed for configuring subpaths and mTLS

```
#Ingress: {
	subpath: {
		prefix:       *"/.*" | string
		prefix_regex: *true | bool
		rewrite:      #Rewrite
	}
	certrequired: *false | bool
}

#Rewrite: {
	pattern:       *"" | string
	rewrite_regex: *false | bool
	substitution:  *"" | string
}
```

It must be used within a connector metadata as follows: 

```
connect: {
  c_example: {
    as: "lb"
    from: inbound: "inbound"
    to: httptest: "main": {
      meta: inb_utils.#InboundMetadata & {
        "__ingress": {   <1>
          subpath: {
            prefix:       "/baz/"   <2>
            prefix_regex: false     <3>
            rewrite: {
              substitution:  ""     <4>
              pattern:       ""     <5>
              rewrite_regex: false  <6>
            }
          }
          certrequired: false       <7>
        }
      }
    }
  }
}
```

1. These features get specified under ___ingress_ within a connector _meta_ field.
2. Defines the prefix that must be matched to route traffic to this service.
3. Indicates that the prefix is not a regex.
4. Indicates what the prefix should be substituted with.
5. Indicates the pattern that must be matched for being substituted.
6. Indicates whether the substitution and patter are specified as regex or just plain text.
7. If client certificate validation is activated at the inbound and _certrequired_ is set to _false_ at the inbound level, this field, at the connector level, controls if mTLS is required or not for the given path.

"__ingress" is the field in the metadata that specifies the parameters of the mapping.

Metadata contents are out of the service model. When we use it to introduce a feature like this, we should advise users not to employ a subset of the possible top level field names in user metadata, and reserve them for us.

We could reserve all fields whose name starts with "__" for features like this, while we consider transitioning them elsewhere, or introduce them in the model. Even when in the model, we can be compatible converting model spec into metadata fields, if we reserve them for the meaning we provide (like in this case)

=== Emissary api version: v2 vs v3alpha1

KuInbound generates resources for Emissary using it v3alpha1 api version. +
But there are some resources (for example `mappings`) that are converted to
the v2 api version (via the emissary-apiext deployment, that needs to be running
at all times). +
*The v2 api version is deprecated*, so we use directly the v3alpha1 api.

So, for example, when KuInbound creates a v3alpha1 `mapping`, it is stored as
v2 `mapping` (in this case, the `hostname` field is changed to `host`).

WARN: From Emissary documentation:
```
There is a known issue with the emissary-apiext service that impacts all Emissary-ingress
2.x and 3.x users. Specifically, the TLS certificate used by apiext expires one
year after creation and does not auto-renew. All users who are running
Emissary-ingress/Ambassador Edge Stack 2.x or 3.x with the apiext service should
proactively renew their certificate as soon as practical by running
"kubectl delete --all secrets --namespace=emissary-system" to delete the existing
certificate, and then restart the emissary-apiext deployment with kubectl rollout
restart deploy/emissary-apiext -n emissary-system. This will create a new certificate
with a one year expiration. We will issue a software patch to address this issue well
before the one year expiration. Note that certificate renewal will not cause any
downtime.
```

=== False *WARN* records in KuInbound logs

When a solution contains an inbound and a regular service, the problem described in https://gitlab.com/kumori/kv3/project-management/-/issues/803 occur. +
Why? Because the kubernetes service must be created by other controller (KuController)

```
INFO[2022-02-16T16:09:10+01:00] kukulinkcontroller.SyncHandler() . InboundConfig: https, ClientCert: false, Websocket: false, Domain: calc-juanjo.test.kumori.cloud, CertDomain: *.test.kumori.cloud
WARN[2022-02-16T16:09:10+01:00] kukulinkcontroller.SyncHandler()  Error getting kubeService service "cinbound-0-kd-160910-d82fbdf3-lb" not found
WARN[2022-02-16T16:09:10+01:00] kukulinkcontroller.processNextWorkItem() error syncing service "cinbound-0-kd-160910-d82fbdf3-lb" not found
```