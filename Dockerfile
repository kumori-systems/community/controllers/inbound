################################################################################
##  KUMORI WARNING: This Dockerfile is used by CI processes, so any changes   ##
##                  should make sure the CI pipelines don't break.            ##
################################################################################

# Build the manager binary
FROM golang:1.19 as builder

# These variables are used by CI to clean up intermediate stage images after
# image generation.
ARG DELETE_LABEL=default
ARG CI_JOB_TOKEN
LABEL deletemark="${DELETE_LABEL}"
ENV DEBIAN_FRONTEND noninteractive

WORKDIR /workspace

# Allow access to kumori gitlab
RUN mkdir -p /root/.ssh
RUN touch /root/.ssh/known_hosts
RUN ssh-keyscan gitlab.com 2> /dev/null | tee -a /root/.ssh/known_hosts > /dev/null 2>&1
RUN echo -e "machine gitlab.com\nlogin gitlab-ci-token\npassword ${CI_JOB_TOKEN}" > /root/.netrc
RUN cat /root/.netrc

# Optionally add a private key for accessing private Git repositories (mostly
# for DEV or private dependencies)
# COPY ecloud_deployment_key /root/.ssh/id_rsa
# RUN chmod 0600 /root/.ssh/id_rsa
# COPY .gitconfig /root/.gitconfig

# Copy the Go Modules manifests
COPY go.mod go.mod
COPY go.sum go.sum
# cache deps before building and copying source so that we don't need to re-download as much
# and so that source changes don't invalidate our downloaded layer
# GOPROXY="direct" is used to avoid problems with GO packages cached, when a
# tag is moved in the gitlab repository
RUN GOPRIVATE="gitlab.com/kumori/*,gitlab.com/kumori-systems/*" go mod download

# Copy the go source
COPY cmd/ cmd/
COPY pkg/ pkg/

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=on go build -a -o manager cmd/controller-manager/main.go

# Use distroless as minimal base image to package the manager binary
# Refer to https://github.com/GoogleContainerTools/distroless for more details
FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/manager .
USER nonroot:nonroot

ENTRYPOINT ["/manager"]
