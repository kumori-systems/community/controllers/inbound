/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kukulinkcontroller

import (
	"encoding/json"
	"fmt"
	hosttools "httpinbound-controller/pkg/utils/ambassador/host"
	mappingtools "httpinbound-controller/pkg/utils/ambassador/mapping"
	tcpmappingtools "httpinbound-controller/pkg/utils/ambassador/tcpmapping"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	kukucatools "httpinbound-controller/pkg/utils/kuku/kukuca"
	kukucerttools "httpinbound-controller/pkg/utils/kuku/kukucert"
	kukudomaintools "httpinbound-controller/pkg/utils/kuku/kukudomain"
	kukulinktools "httpinbound-controller/pkg/utils/kuku/kukulink"
	kukuporttools "httpinbound-controller/pkg/utils/kuku/kukuport"
	"httpinbound-controller/pkg/utils/kuku/v3deployment"
	v3deploymenttools "httpinbound-controller/pkg/utils/kuku/v3deployment"

	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	naming "httpinbound-controller/pkg/utils/naming"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	ambscheme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned/scheme"
	ambinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/informers/externalversions/getambassador.io/v3alpha1"
	amblisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/listers/getambassador.io/v3alpha1"

	log "github.com/sirupsen/logrus"

	corev1 "k8s.io/api/core/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	labels "k8s.io/apimachinery/pkg/labels"
	kubeinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	kubelisters "k8s.io/client-go/listers/core/v1"
	cache "k8s.io/client-go/tools/cache"
	record "k8s.io/client-go/tools/record"
	workqueue "k8s.io/client-go/util/workqueue"
)

// KukuLinkController is the struct representing the controller
type KukuLinkController struct {
	base.BaseController
	kubeClientset        kubeclientset.Interface
	ambClientset         ambclientset.Interface
	kukuLinkInformer     kumoriinformers.KukuLinkInformer
	kukuLinkLister       kumorilisters.KukuLinkLister
	kukuLinkTools        kukulinktools.KukuLinkTools
	mappingInformer      ambinformers.MappingInformer
	mappingLister        amblisters.MappingLister
	mappingTools         mappingtools.MappingTools
	tcpMappingInformer   ambinformers.TCPMappingInformer
	tcpMappingLister     amblisters.TCPMappingLister
	tcpMappingTools      tcpmappingtools.TCPMappingTools
	hostInformer         ambinformers.HostInformer
	hostLister           amblisters.HostLister
	hostTools            hosttools.HostTools
	v3DeploymentInformer kumoriinformers.V3DeploymentInformer
	v3DeploymentLister   kumorilisters.V3DeploymentLister
	v3DeploymentTools    v3deploymenttools.V3DeploymentTools
	kukuDomainInformer   kumoriinformers.KukuDomainInformer
	kukuDomainLister     kumorilisters.KukuDomainLister
	kukuDomainTools      kukudomaintools.KukuDomainTools
	kukuPortInformer     kumoriinformers.KukuPortInformer
	kukuPortLister       kumorilisters.KukuPortLister
	kukuPortTools        kukuporttools.KukuPortTools
	kukuCertInformer     kumoriinformers.KukuCertInformer
	kukuCertLister       kumorilisters.KukuCertLister
	kukuCertTools        kukucerttools.KukuCertTools
	kukuCaInformer       kumoriinformers.KukuCaInformer
	kukuCaLister         kumorilisters.KukuCaLister
	kukuCaTools          kukucatools.KukuCaTools
	kubeServiceInformer  kubeinformers.ServiceInformer
	kubeServiceLister    kubelisters.ServiceLister
	ambRecorder          record.EventRecorder
	controllerConfig     common.ControllerConfig
}

// NewController returns a new kumori controller
func NewController(
	kubeClientset kubeclientset.Interface,
	ambClientset ambclientset.Interface,
	kukuLinkInformer kumoriinformers.KukuLinkInformer, // Primary
	mappingInformer ambinformers.MappingInformer, // Secondary
	tcpMappingInformer ambinformers.TCPMappingInformer, // Secondary
	hostInformer ambinformers.HostInformer, // Secondary
	v3DeploymentInformer kumoriinformers.V3DeploymentInformer, // Special secondary
	kukuDomainInformer kumoriinformers.KukuDomainInformer, // Special secondary
	kukuPortInformer kumoriinformers.KukuPortInformer, // Special secondary
	kukuCertInformer kumoriinformers.KukuCertInformer, // Terciary
	kukuCaInformer kumoriinformers.KukuCaInformer, // Terciary
	kubeServiceInformer kubeinformers.ServiceInformer, // Terciary
	controllerConfig common.ControllerConfig,
) (c *KukuLinkController) {
	controllerName := "kukulinkcontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 10)
	syncedFunctions[0] = kukuLinkInformer.Informer().HasSynced
	syncedFunctions[1] = mappingInformer.Informer().HasSynced
	syncedFunctions[2] = tcpMappingInformer.Informer().HasSynced
	syncedFunctions[3] = hostInformer.Informer().HasSynced
	syncedFunctions[4] = v3DeploymentInformer.Informer().HasSynced
	syncedFunctions[5] = kukuDomainInformer.Informer().HasSynced
	syncedFunctions[6] = kukuPortInformer.Informer().HasSynced
	syncedFunctions[7] = kukuCertInformer.Informer().HasSynced
	syncedFunctions[8] = kukuCaInformer.Informer().HasSynced
	syncedFunctions[9] = kubeServiceInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuLinkInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 6)
	secondarySharedInformers[0] = mappingInformer.Informer()
	secondarySharedInformers[1] = tcpMappingInformer.Informer()
	secondarySharedInformers[2] = hostInformer.Informer()
	secondarySharedInformers[3] = v3DeploymentInformer.Informer()
	secondarySharedInformers[4] = kukuDomainInformer.Informer()
	secondarySharedInformers[5] = kukuPortInformer.Informer()

	recorder := base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName)
	ambRecorder := base.CreateRecorder(kubeClientset, ambscheme.AddToScheme, controllerName)

	kukuLinkLister := kukuLinkInformer.Lister()
	mappingLister := mappingInformer.Lister()
	tcpMappingLister := tcpMappingInformer.Lister()
	hostLister := hostInformer.Lister()
	v3DeploymentLister := v3DeploymentInformer.Lister()
	kukuDomainLister := kukuDomainInformer.Lister()
	kukuPortLister := kukuPortInformer.Lister()
	kukuCertLister := kukuCertInformer.Lister()
	kukuCaLister := kukuCaInformer.Lister()
	kubeServiceLister := kubeServiceInformer.Lister()

	kukuLinkTools := kukulinktools.NewKukuLinkTools(v3DeploymentLister)
	mappingTools := mappingtools.NewMappingTools(ambClientset, mappingLister, ambRecorder)
	tcpMappingTools := tcpmappingtools.NewTCPMappingTools(ambClientset, tcpMappingLister, ambRecorder)
	hostTools := hosttools.NewHostTools(ambClientset, hostLister, ambRecorder)
	v3DeploymentTools := v3deploymenttools.NewV3DeploymentTools(v3DeploymentLister, kukuLinkLister)
	kukuDomainTools := kukudomaintools.NewKukuDomainTools(kukuDomainLister, v3DeploymentLister, kukuLinkLister)
	kukuPortTools := kukuporttools.NewKukuPortTools(kukuPortLister, v3DeploymentLister)
	kukuCertTools := kukucerttools.NewKukuCertTools(kukuCertLister, v3DeploymentLister)
	kukuCaTools := kukucatools.NewKukuCaTools(kukuCaLister, v3DeploymentLister)

	queue := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(),
		controllerName,
	)

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     common.KindKukuLink,
		Workqueue:                queue,
		Recorder:                 recorder,
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}
	c = &KukuLinkController{
		BaseController:       baseController,
		kubeClientset:        kubeClientset,
		ambClientset:         ambClientset,
		kukuLinkInformer:     kukuLinkInformer,
		kukuLinkLister:       kukuLinkLister,
		kukuLinkTools:        kukuLinkTools,
		mappingInformer:      mappingInformer,
		mappingLister:        mappingLister,
		mappingTools:         mappingTools,
		tcpMappingInformer:   tcpMappingInformer,
		tcpMappingLister:     tcpMappingLister,
		tcpMappingTools:      tcpMappingTools,
		hostInformer:         hostInformer,
		hostLister:           hostLister,
		hostTools:            hostTools,
		v3DeploymentInformer: v3DeploymentInformer,
		v3DeploymentLister:   v3DeploymentLister,
		v3DeploymentTools:    v3DeploymentTools,
		kukuDomainInformer:   kukuDomainInformer,
		kukuDomainLister:     kukuDomainLister,
		kukuDomainTools:      kukuDomainTools,
		kukuPortInformer:     kukuPortInformer,
		kukuPortLister:       kukuPortLister,
		kukuPortTools:        kukuPortTools,
		kukuCertInformer:     kukuCertInformer,
		kukuCertLister:       kukuCertLister,
		kukuCertTools:        kukuCertTools,
		kukuCaInformer:       kukuCaInformer,
		kukuCaLister:         kukuCaLister,
		kukuCaTools:          kukuCaTools,
		kubeServiceInformer:  kubeServiceInformer,
		kubeServiceLister:    kubeServiceLister,
		ambRecorder:          ambRecorder,
		controllerConfig:     controllerConfig,
	}
	// We use this "two-direction relationshipt" to implement abstract methods
	// See base/controller.go and this article:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	c.BaseController.IBaseController = c
	return
}

// Reconfig receives a new configuration for the controller: store it and
// reevaluate all primary objects
func (c *KukuLinkController) Reconfig(config common.ControllerConfig) {
	meth := c.Name + ".Reconfig()"
	c.controllerConfig = config
	namespace := ""
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	kukulinks, err := c.kukuLinkLister.KukuLinks(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get KukuLinks to apply a config file change. Error: %s", meth, err.Error())
	}
	if kukulinks != nil {
		for _, kukulink := range kukulinks {
			log.Debugf("%s enqueue due to reconfig %s", meth, kukulink.GetName())
			c.EnqueueObject(kukulink)
		}
	}
}

// HandleSecondary is in charge of processing changes in the secondary objects
// (mappings, tlscontexts) and force to re-evaluate the primary object (kukulink)
// There are special secondary: kukuport, kukudomain and v3dep. Its owner is not the
// kukulink, but we want the kukulink to be re-evaluated if the kukuport or
// kukudomain or v3dep using it is changed.
func (c *KukuLinkController) HandleSecondary(obj interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)

	object, kukuLinkName, err := c.GetObject(obj)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}
	log.Debugf("%s Processing object: %s", meth, object.GetName())

	switch object.(type) {

	//
	// Ports
	//
	case *kumoriv1.KukuPort:
		kukuPort := object.(*kumoriv1.KukuPort)
		err = c.handleKukuPortChange(kukuPort)
		if err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}

	//
	// Domains
	//
	case *kumoriv1.KukuDomain:
		kukuDomain := object.(*kumoriv1.KukuDomain)
		err = c.handleKukuDomainChange(kukuDomain)
		if err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}

	//
	// V3Dep
	//
	case *kumoriv1.V3Deployment:
		v3dep := object.(*kumoriv1.V3Deployment)
		err = c.handleV3DepChange(v3dep)
		if err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}

	default:
		if kukuLinkName != "" {
			log.Debugf("%s Processing KukuLink: %s", meth, object.GetName())
			kukuLink, err := c.kukuLinkLister.KukuLinks(object.GetNamespace()).Get(kukuLinkName)
			if errors.IsNotFound(err) {
				log.Infof("%s ignoring orphaned object '%s' of KukuLink '%s'", meth, object.GetSelfLink(), kukuLinkName)
				return
			} else if err != nil {
				log.Warnf("%s error retrieving object %s: %v", meth, kukuLinkName, err)
			}
			log.Debugf("%s enqueue %s", meth, kukuLink.Name)
			c.EnqueueObject(kukuLink)
		}
	}
}

// SyncHandler compares the actual state with the desired, and attempts to
// converge the two.
func (c *KukuLinkController) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler() "
	log.Infof("%s %s", meth, key)

	// Errors have occurred that have not aborted the processing, but imply that
	// the primary object must be requeued
	pendingRetryErrors := false

	// Retrieve kukulink object
	kukuLink, err := c.getKukuLink(key)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if shouldAbort {
		return errToReturn
	}

	// Check if KukuLink is being deleted (if it has a deletion timestamp set)
	if !kukuLink.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. KukuLink has a deletion timestamp set. Skipping.", meth)
		return nil
	}

	// Get the v3dep-and-channels related to each part of link
	v3Dep1, ch1, v3Dep2, ch2, err := c.kukuLinkTools.GetLinkDeployments(kukuLink)
	if err != nil {
		log.Warnf("%s Error when getting deployments (%s)", meth, err.Error())
	}
	shouldAbort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if shouldAbort {
		return errToReturn
	}

	// This controller should only process the links between an inbound and a
	// regular deployment
	var inboundV3Dep, regularV3Dep *kumoriv1.V3Deployment
	var inboundV3DepChannel, regularV3DepChannel string
	v3Dep1IsInbound := inboundconfig.IsInbound(v3Dep1, c.controllerConfig.InboundVersions)
	v3Dep2IsInbound := inboundconfig.IsInbound(v3Dep2, c.controllerConfig.InboundVersions)

	if !v3Dep1IsInbound && !v3Dep2IsInbound {
		return nil
	} else if v3Dep1IsInbound && v3Dep2IsInbound {
		log.Errorf("%s. Both deployments included in KukuLink are inbonds. Skipping.", meth)
		return nil
	} else if v3Dep1IsInbound {
		inboundV3Dep = v3Dep1
		inboundV3DepChannel = ch1
		regularV3Dep = v3Dep2
		regularV3DepChannel = ch2
	} else {
		inboundV3Dep = v3Dep2
		inboundV3DepChannel = ch2
		regularV3Dep = v3Dep1
		regularV3DepChannel = ch1
	}

	// Get inbound configuration, from the v3dep configuration (params and resources)
	// If an error occur, the v3dep must be requeued
	inboundConfig, err := inboundconfig.NewInboundConfig(
		inboundV3Dep,
		c.kukuDomainTools,
		c.kukuCertTools,
		c.kukuCaTools,
		c.kukuPortTools,
	)
	if err != nil {
		log.Warnf("%s Error getting inbound config %s", meth, err.Error())
		return err
	}
	log.Infof("%s. InboundConfig: %s", meth, inboundConfig.ToString())

	services, err :=
		c.getKubeServicesNamePortAndMeta(regularV3Dep, regularV3DepChannel)
	if err != nil {
		log.Warnf("%s Error getting kubeServices %s", meth, err.Error())
		return err
	}

	if inboundConfig.IsHttpInbound() {
		err = c.syncHandlerHTTPInboundLink(
			kukuLink, inboundConfig.HttpInbound,
			services,
			inboundV3Dep, inboundV3DepChannel,
			regularV3Dep, regularV3DepChannel,
			&pendingRetryErrors,
		)
	} else if inboundConfig.IsTcpInbound() {
		err = c.syncHandlerTCPInboundLink(
			kukuLink, inboundConfig.TcpInbound,
			services,
			inboundV3Dep, inboundV3DepChannel,
			regularV3Dep, regularV3DepChannel,
			&pendingRetryErrors,
		)
	} else {
		err = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid inbound type: %s", inboundConfig.InboundType))
	}

	abort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// If an error has occurred during the process (without abort it), we return
	// an error so the kukulink is requeued
	if pendingRetryErrors {
		return fmt.Errorf("non-critical errors ocurred processing %s", key)
	}

	return nil
}

// handleKukuPortChange enqueues the KukuLink using the kukuPort resource
func (c *KukuLinkController) handleKukuPortChange(kukuPort *kumoriv1.KukuPort) (err error) {
	meth := c.Name + ".handleKukuPortChange()"
	log.Debugf(meth)

	// Look for the v3deployment using the kukuPort
	v3Dep, err := c.v3DeploymentTools.GetV3DeploymentByPort(kukuPort)
	if err != nil {
		return
	}
	if v3Dep == nil {
		return
	}

	// Loof for the kukulink using the v3Dep
	kukuLinks, err := c.v3DeploymentTools.GetLinks(v3Dep)
	if err != nil {
		return
	}
	if len(kukuLinks) == 0 {
		return
	}

	for _, kukuLink := range kukuLinks {
		log.Debugf("%s enqueue %s", meth, kukuLink.Name)
		c.EnqueueObject(kukuLink)
	}

	return
}

// handleKukuDomainChange enqueues the KukuLink if links an inbound using a given KukuDomain resource
func (c *KukuLinkController) handleKukuDomainChange(kukuDomain *kumoriv1.KukuDomain) (err error) {
	meth := c.Name + ".handleKukuDomainChange()"
	log.Debugf(meth)

	kukuDomainName := kukuDomain.GetName()

	kukulink, err := c.kukuDomainTools.GetLink(kukuDomain)
	if errors.IsNotFound(err) || kukulink == nil {
		log.Infof("%s ignoring kukudomain '%s'. KukuLink not found", meth, kukuDomainName)
		err = nil // This case is not an error; just a vhost not used yet
		return
	} else if err != nil {
		log.Warnf("%s error retrieving KukuLink using KukuDomain %s: %v", meth, kukuDomainName, err)
		return
	}

	// Enqueues the KukuLink found
	log.Debugf("%s enqueue %s", meth, kukulink.Name)
	c.EnqueueObject(kukulink)

	return
}

// handleV3DepChange enqueues the KukuLink if links an inbound using a given v3dep
func (c *KukuLinkController) handleV3DepChange(v3dep *kumoriv1.V3Deployment) (err error) {
	meth := c.Name + ".handleV3DepChange(%s)"
	log.Debugf(meth, v3dep.GetName())

	kukulinks, err := c.v3DeploymentTools.GetLinks(v3dep)
	for _, kukulink := range kukulinks {
		if errors.IsNotFound(err) || kukulink == nil {
			log.Infof("%s ignoring v3dep '%s'. KukuLink not found", meth, v3dep.GetName())
			err = nil // This case is not an error; just a vhost not used yet
			return
		} else if err != nil {
			log.Warnf("%s error retrieving KukuLink using v3dep %s: %v", meth, v3dep.GetName(), err)
			return
		}

		// Enqueues the KukuLink found
		log.Debugf("%s enqueue %s", meth, kukulink.Name)
		c.EnqueueObject(kukulink)
	}

	return
}

// getKukuLink retrieve the getKukuLink object from its workqueue key
func (c *KukuLinkController) getKukuLink(
	key string,
) (
	kukulink *kumoriv1.KukuLink, kerr error,
) {
	meth := c.Name + ".getKukuLink() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid resource key: %s", key))
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	kukulink, err = c.kukuLinkLister.KukuLinks(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("'%s' in work queue no longer exists", key))
			log.Warnf("%s %s", meth, kerr.Error())
		} else {
			kerr = kerrors.NewKukuError(kerrors.Retry, err)
			log.Errorf("%s %s", meth, kerr.Error())
		}
		return
	}
	return
}

// getKubeServicesNamePortAndMeta returns a list of KubeServiceInfo which contain
// informaction about the kubeservice name, port number and metadata related
// on how to configure its mapping related to the regular v3deployment and channel.
// Important: function naming.CalculateKubeServiceName() should be the same
// as the function used by KuController.
// TBD: remove the naming.CalculateKubeServiceName() : shoulb be fixed in
// manifests (or by Admission) and not recalculated for several controllers.
func (c *KukuLinkController) getKubeServicesNamePortAndMeta(
	v3Deployment *kumoriv1.V3Deployment, channel string,
) (
	[]v3deployment.KubeServiceInfo, error,
) {
	var services []v3deployment.KubeServiceInfo
	if v3Deployment != nil {
		connectors, err := c.getConnectorsForChannelV3(v3Deployment, channel)
		if err != nil {
			return services, err
		}
		// For now, tags ("multiple version role") are not taken into account,
		// and the default tag (tag=0) is assumed
		for _, connector := range connectors {
			tag := "0"
			kubeServiceName := naming.CalculateKubeServiceName(v3Deployment.Name, connector.Name, tag)
			kubeService, err := c.getKubeService(kubeServiceName, v3Deployment.Namespace)
			if err != nil {
				return services, err
			}
			kubeServicePort := int(kubeService.Spec.Ports[0].Port)
			services = append(services, v3deployment.KubeServiceInfo{Name: kubeServiceName, Port: kubeServicePort, Meta: connector.Meta})
		}
		return services, nil
	} else {
		return services, fmt.Errorf("v3deployment are nil")
	}
}

func (c *KukuLinkController) getConnectorsForChannelV3(
	v3Deployment *kumoriv1.V3Deployment,
	channelName string,
) (
	connectorsInfo []v3deployment.ConnectorInfo, err error,
) {
	// Search the connectors with a or-duplex channel named "channelName",
	// whose role is "self".
	// For now, neither duplex channels or tags ("multiple version role") are
	// not taken into account

	if (v3Deployment.Spec.Artifact.Description == nil) ||
		(v3Deployment.Spec.Artifact.Description.Connectors == nil) {
		err = fmt.Errorf("link not found for channel (%s)", channelName)
		return
	}

	connectors := v3Deployment.Spec.Artifact.Description.Connectors
	for k, v := range *connectors {
		for _, client := range v.Clients {
			if client.Role == "self" && client.Channel == channelName {
				connectorsInfo = append(connectorsInfo, v3deployment.ConnectorInfo{Name: k, Meta: c.unmarshallMeta(v)})
			}
		}
	}

	if len(connectorsInfo) == 0 {
		err = fmt.Errorf("links not found for channel (%s)", channelName)
	}

	return
}

func (c *KukuLinkController) unmarshallMeta(v kumoriv1.Connector) v3deployment.ServiceMeta {
	meth := c.Name + ".unmarshallMeta()"
	log.Infof(meth)

	jsonBytes, err := v.Servers[0].Meta.MarshalJSON()
	if err != nil {
		log.Warnf("%s Error marshalling RawExtension to JSON: %v", meth, err)
	}

	var serviceMeta v3deployment.ServiceMeta
	err = json.Unmarshal(jsonBytes, &serviceMeta)
	if err != nil {
		log.Warnf("%s Error unmarshalling JSON to ServiceMeta: %v", meth, err)
	}

	serviceMeta.SetIngressDefaults()

	return serviceMeta
}

func (c *KukuLinkController) getKubeService(
	kubeServiceName string, ns string,
) (
	kubeService *corev1.Service, err error,
) {
	kubeService, err = c.kubeServiceLister.Services(ns).Get(kubeServiceName)
	return
}
