/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kukulinkcontroller

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	"httpinbound-controller/pkg/utils/kuku/v3deployment"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
)

// syncHandlerTcInboundLink compares the actual state with the desired, and
// attempts to converge the two.
func (c *KukuLinkController) syncHandlerTCPInboundLink(
	kukuLink *kumoriv1.KukuLink,
	tcpInboundConfig *inboundconfig.TcpInboundConfig,
	kubeServices []v3deployment.KubeServiceInfo,
	inboundV3Dep *kumoriv1.V3Deployment,
	inboundV3DepChannel string,
	regularV3Dep *kumoriv1.V3Deployment,
	regularV3DepChannel string,
	pendingRetryErrors *bool,
) error {
	meth := c.Name + ".syncHandlerTCPInboundLink()"
	log.Infof("%s link %s", meth, c.kukuLinkTools.LinkToString(kukuLink))

	// ---------------------------------------------------------------------------
	// For each secondary resource (mappings) related to the primary
	// (kukutcpinbound), we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	// Just the first kubeService is taken into account so resulting logic in this area is
	// kept as it was before (https://kumorisystems.visualstudio.com/CMC/_workitems/edit/375)
	mappingsToBeRemoved, mappingsToBeUpdated, mappingsToBeCreated, err :=
		c.getTCPMappingChanges(
			kukuLink,
			tcpInboundConfig,
			kubeServices[0].Name,
			kubeServices[0].Port,
			pendingRetryErrors,
		)

	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(mappingsToBeRemoved) == 0 &&
		len(mappingsToBeUpdated) == 0 &&
		len(mappingsToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"mapping(%d,%d,%d)",
			len(mappingsToBeRemoved),
			len(mappingsToBeUpdated),
			len(mappingsToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (mapping) related to the primary
	// (kukuHttpInbound), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncTCPMapping(
		mappingsToBeRemoved,
		mappingsToBeUpdated,
		mappingsToBeCreated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	return nil
}

// getTCPMappingChanges return the maps of mappings to be removed/updated/created
func (c *KukuLinkController) getTCPMappingChanges(
	kukuLink *kumoriv1.KukuLink,
	tcpInboundConfig *inboundconfig.TcpInboundConfig,
	kubeServiceName string,
	kubeServicePort int,
	pendingRetryErrors *bool,
) (
	mappingsToBeRemoved map[string]*ambv3alpha1.TCPMapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.TCPMapping,
	mappingsToBeCreated map[string]*ambv3alpha1.TCPMapping,
	errToReturn error,
) {
	mappingsToBeRemoved = make(map[string]*ambv3alpha1.TCPMapping)
	mappingsToBeUpdated = make(map[string]*ambv3alpha1.TCPMapping)
	mappingsToBeCreated = make(map[string]*ambv3alpha1.TCPMapping)
	mappingNotChanged := make(map[string]*ambv3alpha1.TCPMapping)

	// Get the map of mappings currently related to kukuhttpinbound (via owner label)
	currentMapping, err := c.tcpMappingTools.GetCurrentMap(kukuLink)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	// Map that should exist (exactly one, for builtin inbound v1.0.0)
	obj := c.tcpMappingTools.GenerateLinkedTCPMapping(
		tcpInboundConfig,
		kukuLink,
		kubeServiceName,
		kubeServicePort,
	)

	name := obj.GetName()
	if _, ok := currentMapping[name]; ok == false {
		mappingsToBeCreated[name] = obj
	} else if !c.tcpMappingTools.Equal(obj, currentMapping[name]) {
		mappingsToBeUpdated[name] = c.tcpMappingTools.MergeToCurrent(currentMapping[name], obj)
	} else {
		mappingNotChanged[name] = obj
	}

	// Get the list of mapping to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed)
	for k, v := range currentMapping {
		_, ok1 := mappingsToBeCreated[k]
		_, ok2 := mappingsToBeUpdated[k]
		_, ok3 := mappingNotChanged[k]
		if !ok1 && !ok2 && !ok3 {
			mappingsToBeRemoved[k] = v
		}
	}

	return
}

// syncTCPMapping executes the provided actions (tcpmappingsToBeRemoved, tcpmappingsToBeUpdated,
// tcpmappingsToBeUpdated,, tcpmappingsToBeCreated) to sync the state of mapping
func (c *KukuLinkController) syncTCPMapping(
	mappingsToBeRemoved map[string]*ambv3alpha1.TCPMapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.TCPMapping,
	mappingsToBeCreated map[string]*ambv3alpha1.TCPMapping,
	kukuLink *kumoriv1.KukuLink,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncTCPMapping()"
	log.Infof(meth)

	err := c.tcpMappingTools.ExecuteActionInMap(
		common.ActionDelete,
		mappingsToBeRemoved,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.tcpMappingTools.ExecuteActionInMap(
		common.ActionUpdate,
		mappingsToBeUpdated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.tcpMappingTools.ExecuteActionInMap(
		common.ActionCreate,
		mappingsToBeCreated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}
