/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package kukulinkcontroller

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	"httpinbound-controller/pkg/utils/kuku/v3deployment"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
)

// syncHandlerHTTPInboundLink compares the actual state with the desired, and
// attempts to converge the two.
func (c *KukuLinkController) syncHandlerHTTPInboundLink(
	kukuLink *kumoriv1.KukuLink,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	kubeServices []v3deployment.KubeServiceInfo,
	inboundV3Dep *kumoriv1.V3Deployment,
	inboundV3DepChannel string,
	regularV3Dep *kumoriv1.V3Deployment,
	regularV3DepChannel string,
	pendingRetryErrors *bool,
) error {
	meth := c.Name + ".syncHandlerHTTPInboundLink()"
	log.Infof("%s link %s", meth, c.kukuLinkTools.LinkToString(kukuLink))

	// ---------------------------------------------------------------------------
	// For each secondary resource (mappings) related to the primary
	// (KukuLink), we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	mappingsToBeRemoved, mappingsToBeUpdated, mappingsToBeCreated, err :=
		c.getHTTPMappingChanges(
			kukuLink,
			httpInboundConfig,
			kubeServices,
			pendingRetryErrors,
		)

	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(mappingsToBeRemoved) == 0 &&
		len(mappingsToBeUpdated) == 0 &&
		len(mappingsToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"mapping(%d,%d,%d)",
			len(mappingsToBeRemoved),
			len(mappingsToBeUpdated),
			len(mappingsToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (mapping) related to the primary
	// (kukuHttpInbound), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncHTTPMapping(
		mappingsToBeRemoved,
		mappingsToBeUpdated,
		mappingsToBeCreated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	return nil
}

// getHTTPMappingChanges return the maps of mappings to be removed/updated/created
func (c *KukuLinkController) getHTTPMappingChanges(
	kukuLink *kumoriv1.KukuLink,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	kubeServices []v3deployment.KubeServiceInfo,
	pendingRetryErrors *bool,
) (
	mappingsToBeRemoved map[string]*ambv3alpha1.Mapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.Mapping,
	mappingsToBeCreated map[string]*ambv3alpha1.Mapping,
	errToReturn error,
) {
	meth := c.Name + ".getHTTPMappingChanges()"
	log.Infof(meth)
	mappingsToBeRemoved = make(map[string]*ambv3alpha1.Mapping)
	mappingsToBeUpdated = make(map[string]*ambv3alpha1.Mapping)
	mappingsToBeCreated = make(map[string]*ambv3alpha1.Mapping)
	mappingNotChanged := make(map[string]*ambv3alpha1.Mapping)
	generatedMappings := make(map[string]*ambv3alpha1.Mapping)

	// Get the map of mappings currently related to kukuLink (via owner label)
	currentMapping, err := c.mappingTools.GetCurrentMap(kukuLink)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	log.Debugf("%s: Kubeservices: %v --- %d", meth, kubeServices, len(kubeServices))

	// Maps that should exist related to a KukuLink
	for _, service := range kubeServices {
		mappingName, obj := c.mappingTools.GenerateLinkedMapping(
			httpInboundConfig,
			kukuLink,
			service.Name,
			service.Port,
			service.Meta,
		)

		generatedMappings[mappingName] = obj
	}

	for _, generatedMapping := range generatedMappings {
		name := generatedMapping.GetName()
		if _, ok := currentMapping[name]; !ok {
			log.Debugf("%s: Mapping to be Created %s --- Prefix %s", meth, name, generatedMapping.Spec.Prefix)
			mappingsToBeCreated[name] = generatedMapping
		} else if !c.mappingTools.Equal(generatedMapping, currentMapping[name]) {
			log.Debugf("%s: Mapping to be Updated %s --- Prefix %s", meth, name, generatedMapping.Spec.Prefix)
			mappingsToBeUpdated[name] = c.mappingTools.MergeToCurrent(currentMapping[name], generatedMapping)
		} else {
			log.Debugf("%s: Mapping to NOT be Changed %s --- Prefix %s", meth, name, generatedMapping.Spec.Prefix)
			mappingNotChanged[name] = generatedMapping
		}
	}

	// Get the list of mapping to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed)
	for k, v := range currentMapping {
		_, ok1 := mappingsToBeCreated[k]
		_, ok2 := mappingsToBeUpdated[k]
		_, ok3 := mappingNotChanged[k]
		if !ok1 && !ok2 && !ok3 {
			log.Debugf("%s: Mapping to be Removed %s --- Prefix %s", meth, k, v.Spec.Prefix)
			mappingsToBeRemoved[k] = v
		}
	}

	return
}

// syncHTTPMapping executes the provided actions (mappingsToBeRemoved, mappingsToBeUpdated,
// mappingsToBeCreated) to sync the state of mapping
func (c *KukuLinkController) syncHTTPMapping(
	mappingsToBeRemoved map[string]*ambv3alpha1.Mapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.Mapping,
	mappingsToBeCreated map[string]*ambv3alpha1.Mapping,
	kukuLink *kumoriv1.KukuLink,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncHTTPMapping()"
	log.Infof(meth)

	err := c.mappingTools.ExecuteActionInMap(
		common.ActionDelete,
		mappingsToBeRemoved,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.mappingTools.ExecuteActionInMap(
		common.ActionUpdate,
		mappingsToBeUpdated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.mappingTools.ExecuteActionInMap(
		common.ActionCreate,
		mappingsToBeCreated,
		kukuLink,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}
