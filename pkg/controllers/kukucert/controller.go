/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package httpinboundcontroller

import (
	"bytes"
	"fmt"

	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	naming "httpinbound-controller/pkg/utils/naming"

	secrettools "httpinbound-controller/pkg/utils/kube/secret"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	labels "k8s.io/apimachinery/pkg/labels"
	kubeinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	kubelisters "k8s.io/client-go/listers/core/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

// KukuCertController is the struct representing the controller
type KukuCertController struct {
	base.BaseController
	kubeClientset    kubeclientset.Interface
	secretInformer   kubeinformers.SecretInformer
	secretLister     kubelisters.SecretLister
	kukuCertInformer kumoriinformers.KukuCertInformer
	kukuCertLister   kumorilisters.KukuCertLister
	kubeSecretTools  secrettools.SecretTools
	controllerConfig common.ControllerConfig
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kukuCertInformer kumoriinformers.KukuCertInformer, // Primary
	secretInformer kubeinformers.SecretInformer, // Secondary
	controllerConfig common.ControllerConfig,
) (c *KukuCertController) {
	controllerName := "kukucertcontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 2)
	syncedFunctions[0] = kukuCertInformer.Informer().HasSynced
	syncedFunctions[1] = secretInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuCertInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 1)
	secondarySharedInformers[0] = secretInformer.Informer()

	recorder := base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName)
	secretLister := secretInformer.Lister()
	kubeSecretTools := secrettools.NewSecretTools(kubeClientset, secretLister, recorder)
	kukuCertLister := kukuCertInformer.Lister()
	queue := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(),
		controllerName,
	)

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     common.KindKukuCert,
		Workqueue:                queue,
		Recorder:                 recorder,
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}
	c = &KukuCertController{
		BaseController:   baseController,
		kubeClientset:    kubeClientset,
		secretInformer:   secretInformer,
		secretLister:     secretLister,
		kukuCertInformer: kukuCertInformer,
		kukuCertLister:   kukuCertLister,
		kubeSecretTools:  kubeSecretTools,
		controllerConfig: controllerConfig,
	}
	// We use this "two-direction relationshipt" to implement abstract methods
	// See base/controller.go and this article:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	c.BaseController.IBaseController = c
	return
}

// Reconfig receives a new configuration for the controller: store it and
// reevaluate all primary objects
func (c *KukuCertController) Reconfig(config common.ControllerConfig) {
	meth := c.Name + ".Reconfig()"
	c.controllerConfig = config
	namespace := ""
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	kukucerts, err := c.kukuCertLister.KukuCerts(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get KukuCerts to apply a config file change. Error: %s", meth, err.Error())
	}
	if kukucerts != nil {
		for _, kukucert := range kukucerts {
			log.Debugf("%s enqueue due to reconfig %s", meth, kukucert.GetName())
			c.EnqueueObject(kukucert)
		}
	}
}

// HandleSecondary is in charge of processing changes in the secondary objects
// (kube-secrets) and force to re-evaluate the primary object (kuku-cert)
func (c *KukuCertController) HandleSecondary(obj interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)

	object, kukuCertName, err := c.GetObject(obj)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	if kukuCertName != "" {
		log.Debugf("%s Processing object: %s", meth, object.GetName())
		kukucert, err := c.kukuCertLister.KukuCerts(object.GetNamespace()).Get(kukuCertName)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukucert '%s'", meth, object.GetSelfLink(), kukuCertName)
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, kukuCertName, err)
		}

		log.Debugf("%s enqueue %s", meth, kukucert.GetName())
		c.EnqueueObject(kukucert)
	}
}

// SyncHandler compares the actual state with the desired, and attempts to
// converge the two.
func (c *KukuCertController) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler() "
	log.Infof("%s %s", meth, key)

	// Errors have occurred that have not aborted the processing, but imply that
	// the primary object must be requeued
	pendingRetryErrors := false

	// Retrieve kukucert object
	kukucert, err := c.getKukuCert(key)
	abort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// Check if KukuCert is being deleted (if it has a deletion timestamp set)
	if !kukucert.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. KukuCert has a deletion timestamp set. Skipping.", meth)
		return nil
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (secret) related to the primary
	// (kukuCert), we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	secretsToBeRemoved, secretsToBeUpdated, secretsToBeCreated, err :=
		c.getSecretChanges(kukucert, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(secretsToBeRemoved) == 0 &&
		len(secretsToBeUpdated) == 0 &&
		len(secretsToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"secrets(%d,%d,%d)",
			len(secretsToBeRemoved),
			len(secretsToBeUpdated),
			len(secretsToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (secret, tlscontext) related to the primary
	// (kukuCert), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncSecret(
		secretsToBeRemoved,
		secretsToBeUpdated,
		secretsToBeCreated,
		kukucert,
		&pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// If an error has occurred during the process (without abort it), we return
	// an error so the kukucert is requeued
	if pendingRetryErrors {
		return fmt.Errorf("non-critical errors ocurred processing %s", key)
	}

	return nil
}

// getSecretChanges return the maps of secrets to be removed, updated and created
func (c *KukuCertController) getSecretChanges(
	kukucert *kumoriv1.KukuCert,
	pendingRetryErrors *bool,
) (
	secretsToBeRemoved map[string]*corev1.Secret,
	secretsToBeUpdated map[string]*corev1.Secret,
	secretsToBeCreated map[string]*corev1.Secret,
	errToReturn error,
) {
	secretsToBeRemoved = make(map[string]*corev1.Secret)
	secretsToBeUpdated = make(map[string]*corev1.Secret)
	secretsToBeCreated = make(map[string]*corev1.Secret)

	// Get the name of the kubesecret (cert) that should have related kukucert
	kukuCertName := kukucert.GetName()
	certSecretName := naming.CalculateCertSecretName(kukuCertName)

	// Get the map of kubesecrets currently related to kukucert (via owner label)
	currentSecrets, err := c.kubeSecretTools.GetCurrentMap(kukucert)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	// Get the map of kubesecrets to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed), and the kubesecrets to be created/updated
	// There must be a kubesecret for the server cert
	foundCertSecret := false
	for k, v := range currentSecrets {
		if k == certSecretName {
			foundCertSecret = true
			if !isExpectedCertSecret(kukucert, v) {
				secretsToBeUpdated[k] = c.kubeSecretTools.GenerateSecretFromCert(kukucert)
			}
		} else {
			secretsToBeRemoved[k] = v
		}
	}
	if !foundCertSecret {
		secretsToBeCreated[certSecretName] = c.kubeSecretTools.GenerateSecretFromCert(kukucert)
	}

	return
}

// syncSecrets checks if the the secret object related to the kukucert
// object exist, and creates it if not exists or updated it if its content is
// not the expected.
func (c *KukuCertController) syncSecret(
	secretsToBeRemoved map[string]*corev1.Secret,
	secretsToBeUpdated map[string]*corev1.Secret,
	secretsToBeCreated map[string]*corev1.Secret,
	kukucert *kumoriv1.KukuCert,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncSecret()"
	log.Infof(meth)

	err := c.kubeSecretTools.ExecuteActionInMap(
		common.ActionDelete,
		secretsToBeRemoved,
		kukucert,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.kubeSecretTools.ExecuteActionInMap(
		common.ActionUpdate,
		secretsToBeUpdated,
		kukucert,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.kubeSecretTools.ExecuteActionInMap(
		common.ActionCreate,
		secretsToBeCreated,
		kukucert,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}

// getKukuCert retrieve the KukuCert object from its workqueue key
func (c *KukuCertController) getKukuCert(key string) (kukucert *kumoriv1.KukuCert, kerr error) {
	meth := c.Name + ".getKukuCert() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid resource key: %s", key))
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	kukucert, err = c.kukuCertLister.KukuCerts(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("'%s' in work queue no longer exists", key))
			log.Warnf("%s %s", meth, kerr.Error())
		} else {
			kerr = kerrors.NewKukuError(kerrors.Retry, err)
			log.Errorf("%s %s", meth, kerr.Error())
		}
		return
	}

	kukucert = kukucert.DeepCopy()
	return
}

// isExpectedCertSecret checks if the kukucert resource is equivalent to the
// kubesecret resource containing a cert secret
func isExpectedCertSecret(kukucert *kumoriv1.KukuCert, secret *corev1.Secret) bool {
	if secret.Type != "kubernetes.io/tls" {
		return false
	}
	if !bytes.Equal(kukucert.Data.Cert, secret.Data["tls.crt"]) {
		return false
	}
	if !bytes.Equal(kukucert.Data.Key, secret.Data["tls.key"]) {
		return false
	}
	return true
}
