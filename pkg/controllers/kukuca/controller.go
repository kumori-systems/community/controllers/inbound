/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package httpinboundcontroller

import (
	"bytes"
	"fmt"

	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	naming "httpinbound-controller/pkg/utils/naming"

	secrettools "httpinbound-controller/pkg/utils/kube/secret"

	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"

	log "github.com/sirupsen/logrus"
	corev1 "k8s.io/api/core/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	kubeinformers "k8s.io/client-go/informers/core/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	kubelisters "k8s.io/client-go/listers/core/v1"
	cache "k8s.io/client-go/tools/cache"
	workqueue "k8s.io/client-go/util/workqueue"
)

// KukuCaController is the struct representing the controller
type KukuCaController struct {
	base.BaseController
	kubeClientset   kubeclientset.Interface
	secretInformer  kubeinformers.SecretInformer
	secretLister    kubelisters.SecretLister
	kukuCaInformer  kumoriinformers.KukuCaInformer
	kukuCaLister    kumorilisters.KukuCaLister
	kubeSecretTools secrettools.SecretTools
}

// NewController returns a new kuku controller
func NewController(
	kubeClientset kubeclientset.Interface,
	kukuCaInformer kumoriinformers.KukuCaInformer, // Primary
	secretInformer kubeinformers.SecretInformer, // Secondary
) (c *KukuCaController) {
	controllerName := "kukucacontroller"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 2)
	syncedFunctions[0] = kukuCaInformer.Informer().HasSynced
	syncedFunctions[1] = secretInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = kukuCaInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 1)
	secondarySharedInformers[0] = secretInformer.Informer()

	recorder := base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName)
	secretLister := secretInformer.Lister()
	kubeSecretTools := secrettools.NewSecretTools(kubeClientset, secretLister, recorder)
	kukuCaLister := kukuCaInformer.Lister()
	queue := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(),
		controllerName,
	)

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     common.KindKukuCA,
		Workqueue:                queue,
		Recorder:                 recorder,
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}
	c = &KukuCaController{
		BaseController:  baseController,
		kubeClientset:   kubeClientset,
		secretInformer:  secretInformer,
		secretLister:    secretLister,
		kukuCaInformer:  kukuCaInformer,
		kukuCaLister:    kukuCaLister,
		kubeSecretTools: kubeSecretTools,
	}
	// We use this "two-direction relationshipt" to implement abstract methods
	// See base/controller.go and this article:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	c.BaseController.IBaseController = c
	return
}

// HandleSecondary is in charge of processing changes in the secondary objects
// (kube-secrets) and force to re-evaluate the primary object (kuku-ca)
func (c *KukuCaController) HandleSecondary(obj interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)

	object, kukuCaName, err := c.GetObject(obj)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}

	if kukuCaName != "" {
		log.Debugf("%s Processing object: %s", meth, object.GetName())
		kukuca, err := c.kukuCaLister.KukuCas(object.GetNamespace()).Get(kukuCaName)

		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukuca '%s'", meth, object.GetSelfLink(), kukuCaName)
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, kukuCaName, err)
		}

		log.Debugf("%s enqueue %s", meth, kukuca.GetName())
		c.EnqueueObject(kukuca)
	}
}

// SyncHandler compares the actual state with the desired, and attempts to
// converge the two.
func (c *KukuCaController) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler() "
	log.Infof("%s %s", meth, key)

	// Errors have occurred that have not aborted the processing, but imply that
	// the primary object must be requeued
	pendingRetryErrors := false

	// Retrieve kukuca object
	kukuca, err := c.getKukuCa(key)
	abort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// Check if KukuCa is being deleted (if it has a deletion timestamp set)
	if !kukuca.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. KukuCa has a deletion timestamp set. Skipping.", meth)
		return nil
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (secret) related to the primary (kukuCa),
	// we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	secretsToBeRemoved, secretsToBeUpdated, secretsToBeCreated, err :=
		c.getSecretChanges(kukuca, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(secretsToBeRemoved) == 0 &&
		len(secretsToBeUpdated) == 0 &&
		len(secretsToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"secrets(%d,%d,%d)",
			len(secretsToBeRemoved),
			len(secretsToBeUpdated),
			len(secretsToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (secret) related to the primary (kukuCa),
	// we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncSecret(
		secretsToBeRemoved,
		secretsToBeUpdated,
		secretsToBeCreated,
		kukuca,
		&pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// If an error has occurred during the process (without abort it), we return
	// an error so the kukuca is requeued
	if pendingRetryErrors {
		return fmt.Errorf("non-critical errors ocurred processing %s", key)
	}

	return nil
}

// getKukuCa retrieve the KukuCa object from its workqueue key
func (c *KukuCaController) getKukuCa(key string) (kukuca *kumoriv1.KukuCa, kerr error) {
	meth := c.Name + ".getKukuCa() "

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid resource key: %s", key))
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	kukuca, err = c.kukuCaLister.KukuCas(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("'%s' in work queue no longer exists", key))
			log.Warnf("%s %s", meth, kerr.Error())
		} else {
			kerr = kerrors.NewKukuError(kerrors.Retry, err)
			log.Errorf("%s %s", meth, kerr.Error())
		}
		return
	}

	kukuca = kukuca.DeepCopy()
	return
}

// getSecretChanges return the maps of secrets to be removed, updated and created
func (c *KukuCaController) getSecretChanges(
	kukuca *kumoriv1.KukuCa,
	pendingRetryErrors *bool,
) (
	secretsToBeRemoved map[string]*corev1.Secret,
	secretsToBeUpdated map[string]*corev1.Secret,
	secretsToBeCreated map[string]*corev1.Secret,
	errToReturn error,
) {
	meth := c.Name + ".getSecretChanges()"
	log.Infof(meth)

	secretsToBeRemoved = make(map[string]*corev1.Secret)
	secretsToBeUpdated = make(map[string]*corev1.Secret)
	secretsToBeCreated = make(map[string]*corev1.Secret)

	// Get the name of the kubesecret (ca) that should have related kukuca
	caSecretName := naming.CalculateCASecretName(kukuca.GetName())

	// Get the map of kubesecretscurrently related to kukuca (via owner label)
	currentSecrets, err := c.kubeSecretTools.GetCurrentMap(kukuca)
	shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if shouldAbort {
		return
	}

	// Get the map of kubesecrets to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed), and the kubesecrets to be created/updated
	// There must be a kubesecret for the CA.
	foundCASecret := false
	for k, v := range currentSecrets {
		if k == caSecretName {
			foundCASecret = true
			if !isExpectedCASecret(kukuca, v) {
				secretsToBeUpdated[k] = c.kubeSecretTools.GenerateSecretFromCA(kukuca)
			}
		} else {
			secretsToBeRemoved[k] = v
		}
	}
	if !foundCASecret {
		secretsToBeCreated[caSecretName] = c.kubeSecretTools.GenerateSecretFromCA(kukuca)
	}

	return
}

// syncSecrets checks if the the secret object related to the kukuca
// object exist, and creates it if not exists or updated it if its content is
// not the expected.
func (c *KukuCaController) syncSecret(
	secretsToBeRemoved map[string]*corev1.Secret,
	secretsToBeUpdated map[string]*corev1.Secret,
	secretsToBeCreated map[string]*corev1.Secret,
	kukuca *kumoriv1.KukuCa,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncSecret()"
	log.Infof(meth)

	err := c.kubeSecretTools.ExecuteActionInMap(
		common.ActionDelete,
		secretsToBeRemoved,
		kukuca,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.kubeSecretTools.ExecuteActionInMap(
		common.ActionUpdate,
		secretsToBeUpdated,
		kukuca,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.kubeSecretTools.ExecuteActionInMap(
		common.ActionCreate,
		secretsToBeCreated,
		kukuca,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}

// isExpectedCASecret checks if the kukuca resource is equivalent to the
// kubesecret resource containing a ca secret
func isExpectedCASecret(kukuca *kumoriv1.KukuCa, secret *corev1.Secret) bool {
	if secret.Type != "Opaque" {
		return false
	}
	if kukuca.Spec.Ca != nil && secret.Data["tls.crt"] != nil {
		if !bytes.Equal(kukuca.Spec.Ca, secret.Data["tls.crt"]) {
			return false
		}
	}
	if (kukuca.Spec.Ca != nil && secret.Data["tls.crt"] == nil) ||
		(kukuca.Spec.Ca == nil && secret.Data["tls.crt"] != nil) {
		return false
	}
	return true
}
