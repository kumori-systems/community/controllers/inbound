/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v3depcontroller

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
)

// SyncHandlerAmbassador compares the actual state with the desired, and
// attempts to converge the two.
func (c *V3DepController) SyncHandlerAmbassador(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	pendingRetryErrors *bool,
) error {
	meth := c.Name + ".SyncHandlerAmbassador()"
	log.Infof(meth)

	// ---------------------------------------------------------------------------
	// For each secondary resource (hosts and mappings) related to
	//  the primary (v3dep), we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	hostsToBeRemoved, hostsToBeUpdated, hostsToBeCreated, err :=
		c.getHostChanges(v3Dep, httpInboundConfig, pendingRetryErrors)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	mappingsToBeRemoved, mappingsToBeUpdated, mappingsToBeCreated, err :=
		c.getMappingChanges(v3Dep, httpInboundConfig, pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(hostsToBeRemoved) == 0 &&
		len(hostsToBeUpdated) == 0 &&
		len(hostsToBeCreated) == 0 &&
		len(mappingsToBeRemoved) == 0 &&
		len(mappingsToBeUpdated) == 0 &&
		len(mappingsToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"host(%d,%d,%d), mapping(%d,%d,%d)",
			len(hostsToBeRemoved),
			len(hostsToBeUpdated),
			len(hostsToBeCreated),
			len(mappingsToBeRemoved),
			len(mappingsToBeUpdated),
			len(mappingsToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (ambassador host and mapping) related to
	// the primary (kukuHttpInbound), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncHost(
		hostsToBeRemoved,
		hostsToBeUpdated,
		hostsToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	err = c.syncMapping(
		mappingsToBeRemoved,
		mappingsToBeUpdated,
		mappingsToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	return nil
}

// getHostChanges return the maps of host to be removed, updated and created
func (c *V3DepController) getHostChanges(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	pendingRetryErrors *bool,
) (
	hostsToBeRemoved map[string]*ambv3alpha1.Host,
	hostsToBeUpdated map[string]*ambv3alpha1.Host,
	hostsToBeCreated map[string]*ambv3alpha1.Host,
	errToReturn error,
) {
	hostsToBeRemoved = make(map[string]*ambv3alpha1.Host)
	hostsToBeUpdated = make(map[string]*ambv3alpha1.Host)
	hostsToBeCreated = make(map[string]*ambv3alpha1.Host)
	hostsNotChanged := make(map[string]*ambv3alpha1.Host)

	// Get the map of host currently related to v3dep (via owner label)
	currentHosts, err := c.hostTools.GetCurrentMap(v3Dep)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	// Create the host that should exist (exactly one)
	obj := c.hostTools.GenerateHost(
		v3Dep,
		httpInboundConfig,
		c.controllerConfig,
	)

	// Get the list of host to be updated/created
	if obj != nil {
		name := obj.GetName()
		if _, ok := currentHosts[name]; !ok {
			hostsToBeCreated[name] = obj
		} else if !c.hostTools.Equal(obj, currentHosts[name]) {
			hostsToBeUpdated[name] = c.hostTools.MergeToCurrent(currentHosts[name], obj)
		} else {
			hostsNotChanged[name] = obj
		}
	}

	// Get the list of host to be removed (only possible if we are updating
	// the controller and naming algorithm changes)
	for k, v := range currentHosts {
		_, ok1 := hostsToBeCreated[k]
		_, ok2 := hostsToBeUpdated[k]
		_, ok3 := hostsNotChanged[k]
		if !ok1 && !ok2 && !ok3 {
			hostsToBeRemoved[k] = v
		}
	}

	return
}

// getMappingChanges return the maps of mappings to be removed, updated and created
func (c *V3DepController) getMappingChanges(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	pendingRetryErrors *bool,
) (
	mappingsToBeRemoved map[string]*ambv3alpha1.Mapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.Mapping,
	mappingsToBeCreated map[string]*ambv3alpha1.Mapping,
	errToReturn error,
) {
	mappingsToBeRemoved = make(map[string]*ambv3alpha1.Mapping)
	mappingsToBeUpdated = make(map[string]*ambv3alpha1.Mapping)
	mappingsToBeCreated = make(map[string]*ambv3alpha1.Mapping)
	mappingNotChanged := make(map[string]*ambv3alpha1.Mapping)

	// Get the map of mappings currently related to v3dep (via owner label)
	currentMapping, err := c.mappingTools.GetCurrentMap(v3Dep)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	// Map that should exist (exactly one, for builtin inbound v1.0.0)
	obj := c.mappingTools.GenerateUnlinkedMapping(
		v3Dep,
		httpInboundConfig,
	)

	name := obj.GetName()
	if _, ok := currentMapping[name]; !ok {
		mappingsToBeCreated[name] = obj
	} else if !c.mappingTools.Equal(obj, currentMapping[name]) {
		mappingsToBeUpdated[name] = c.mappingTools.MergeToCurrent(currentMapping[name], obj)
	} else {
		mappingNotChanged[name] = obj
	}

	// Get the list of mapping to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed)
	for k, v := range currentMapping {
		_, ok1 := mappingsToBeCreated[k]
		_, ok2 := mappingsToBeUpdated[k]
		_, ok3 := mappingNotChanged[k]
		if !ok1 && !ok2 && !ok3 {
			mappingsToBeRemoved[k] = v
		}
	}

	return
}

// syncHost checks if the the exactly one Host object related to the v3dep
// object exists, and creates it if not exists or updated it if its content is
// not the expected.
func (c *V3DepController) syncHost(
	hostsToBeRemoved map[string]*ambv3alpha1.Host,
	hostsToBeUpdated map[string]*ambv3alpha1.Host,
	hostsToBeCreated map[string]*ambv3alpha1.Host,
	v3Dep *kumoriv1.V3Deployment,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncHost()"
	log.Infof(meth)

	err := c.hostTools.ExecuteActionInMap(
		common.ActionDelete,
		hostsToBeRemoved,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.hostTools.ExecuteActionInMap(
		common.ActionUpdate,
		hostsToBeUpdated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.hostTools.ExecuteActionInMap(
		common.ActionCreate,
		hostsToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}

// syncMapping executes the provided actions (mappingsToBeRemoved, mappingsToBeUpdated,
// mappingsToBeCreated) to sync the state of mapping
func (c *V3DepController) syncMapping(
	mappingsToBeRemoved map[string]*ambv3alpha1.Mapping,
	mappingsToBeUpdated map[string]*ambv3alpha1.Mapping,
	mappingsToBeCreated map[string]*ambv3alpha1.Mapping,
	v3Dep *kumoriv1.V3Deployment,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncMapping()"
	log.Infof(meth)

	err := c.mappingTools.ExecuteActionInMap(
		common.ActionDelete,
		mappingsToBeRemoved,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.mappingTools.ExecuteActionInMap(
		common.ActionUpdate,
		mappingsToBeUpdated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.mappingTools.ExecuteActionInMap(
		common.ActionCreate,
		mappingsToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}
