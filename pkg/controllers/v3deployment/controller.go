/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v3depcontroller

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"

	log "github.com/sirupsen/logrus"
	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	hosttools "httpinbound-controller/pkg/utils/ambassador/host"
	mappingtools "httpinbound-controller/pkg/utils/ambassador/mapping"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	ingresstools "httpinbound-controller/pkg/utils/kube/ingress"
	kukucatools "httpinbound-controller/pkg/utils/kuku/kukuca"
	kukucerttools "httpinbound-controller/pkg/utils/kuku/kukucert"
	kukudomaintools "httpinbound-controller/pkg/utils/kuku/kukudomain"
	kukuporttools "httpinbound-controller/pkg/utils/kuku/kukuport"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorischeme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned/scheme"
	kumoriinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	ambscheme "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned/scheme"
	ambinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/informers/externalversions/getambassador.io/v3alpha1"
	amblisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/listers/getambassador.io/v3alpha1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	labels "k8s.io/apimachinery/pkg/labels"
	kubenetworkinginformers "k8s.io/client-go/informers/networking/v1"
	kubeclientset "k8s.io/client-go/kubernetes"
	kubenetworkinglisters "k8s.io/client-go/listers/networking/v1"
	cache "k8s.io/client-go/tools/cache"
	record "k8s.io/client-go/tools/record"
	workqueue "k8s.io/client-go/util/workqueue"
)

// V3DepController is the struct representing the controller
type V3DepController struct {
	base.BaseController
	kubeClientset      kubeclientset.Interface
	ambClientset       ambclientset.Interface
	v3DepInformer      kumoriinformers.V3DeploymentInformer
	v3DepLister        kumorilisters.V3DeploymentLister
	ingressInformer    kubenetworkinginformers.IngressInformer
	ingressLister      kubenetworkinglisters.IngressLister
	ingressTools       ingresstools.IngressTools
	hostInformer       ambinformers.HostInformer
	hostLister         amblisters.HostLister
	hostTools          hosttools.HostTools
	mappingInformer    ambinformers.MappingInformer
	mappingLister      amblisters.MappingLister
	mappingTools       mappingtools.MappingTools
	kukuDomainInformer kumoriinformers.KukuDomainInformer
	kukuDomainLister   kumorilisters.KukuDomainLister
	kukuDomainTools    kukudomaintools.KukuDomainTools
	kukuCertInformer   kumoriinformers.KukuCertInformer
	kukuCertLister     kumorilisters.KukuCertLister
	kukuCertTools      kukucerttools.KukuCertTools
	kukuCaInformer     kumoriinformers.KukuCaInformer
	kukuCaLister       kumorilisters.KukuCaLister
	kukuCaTools        kukucatools.KukuCaTools
	kukuPortInformer   kumoriinformers.KukuPortInformer
	kukuPortLister     kumorilisters.KukuPortLister
	kukuPortTools      kukuporttools.KukuPortTools
	kukuLinkInformer   kumoriinformers.KukuLinkInformer
	kukuLinkLister     kumorilisters.KukuLinkLister
	ambRecorder        record.EventRecorder
	controllerConfig   common.ControllerConfig
}

// NewController returns a new kumori controller
func NewController(
	kubeClientset kubeclientset.Interface,
	ambClientset ambclientset.Interface,
	v3DepInformer kumoriinformers.V3DeploymentInformer, // Primary
	ingressInformer kubenetworkinginformers.IngressInformer, // Secondary
	hostInformer ambinformers.HostInformer, // Secondary
	mappingInformer ambinformers.MappingInformer, // Secondary
	kukuDomainInformer kumoriinformers.KukuDomainInformer, // Special secondary
	kukuCertInformer kumoriinformers.KukuCertInformer, // Special secondary
	kukuCaInformer kumoriinformers.KukuCaInformer, // Special secondary
	kukuPortInformer kumoriinformers.KukuPortInformer, // Special secondary
	kukuLinkInformer kumoriinformers.KukuLinkInformer, // Terciary
	controllerConfig common.ControllerConfig,
) (c *V3DepController) {
	controllerName := "V3DepController"
	meth := controllerName + ".NewController()"
	log.Infof("%s Creating controller", meth)

	syncedFunctions := make([]cache.InformerSynced, 9)
	syncedFunctions[0] = v3DepInformer.Informer().HasSynced
	syncedFunctions[1] = ingressInformer.Informer().HasSynced
	syncedFunctions[2] = hostInformer.Informer().HasSynced
	syncedFunctions[3] = mappingInformer.Informer().HasSynced
	syncedFunctions[4] = kukuDomainInformer.Informer().HasSynced
	syncedFunctions[5] = kukuCertInformer.Informer().HasSynced
	syncedFunctions[6] = kukuCaInformer.Informer().HasSynced
	syncedFunctions[7] = kukuPortInformer.Informer().HasSynced
	syncedFunctions[8] = kukuLinkInformer.Informer().HasSynced

	primarySharedInformers := make([]cache.SharedIndexInformer, 1)
	primarySharedInformers[0] = v3DepInformer.Informer()

	secondarySharedInformers := make([]cache.SharedIndexInformer, 7)
	secondarySharedInformers[0] = ingressInformer.Informer()
	secondarySharedInformers[1] = hostInformer.Informer()
	secondarySharedInformers[2] = mappingInformer.Informer()
	secondarySharedInformers[3] = kukuDomainInformer.Informer()
	secondarySharedInformers[4] = kukuCertInformer.Informer()
	secondarySharedInformers[5] = kukuCaInformer.Informer()
	secondarySharedInformers[6] = kukuPortInformer.Informer()

	recorder := base.CreateRecorder(kubeClientset, kumorischeme.AddToScheme, controllerName)
	ambRecorder := base.CreateRecorder(kubeClientset, ambscheme.AddToScheme, controllerName)

	v3DepLister := v3DepInformer.Lister()
	ingressLister := ingressInformer.Lister()
	hostLister := hostInformer.Lister()
	mappingLister := mappingInformer.Lister()
	kukuDomainLister := kukuDomainInformer.Lister()
	kukuCertLister := kukuCertInformer.Lister()
	kukuCaLister := kukuCaInformer.Lister()
	kukuPortLister := kukuPortInformer.Lister()
	kukuLinkLister := kukuLinkInformer.Lister()

	ingressTools := ingresstools.NewIngressTools(kubeClientset, ingressLister, recorder)
	mappingTools := mappingtools.NewMappingTools(ambClientset, mappingLister, ambRecorder)
	hostTools := hosttools.NewHostTools(ambClientset, hostLister, ambRecorder)
	kukuDomainTools := kukudomaintools.NewKukuDomainTools(kukuDomainLister, v3DepLister, kukuLinkLister)
	kukuCertTools := kukucerttools.NewKukuCertTools(kukuCertLister, v3DepLister)
	kukuCaTools := kukucatools.NewKukuCaTools(kukuCaLister, v3DepLister)
	kukuPortTools := kukuporttools.NewKukuPortTools(kukuPortLister, v3DepLister)

	queue := workqueue.NewNamedRateLimitingQueue(
		workqueue.DefaultControllerRateLimiter(),
		controllerName,
	)

	baseController := base.BaseController{
		Name:                     controllerName,
		Kind:                     common.KindV3Deployment,
		Workqueue:                queue,
		Recorder:                 recorder,
		SyncedFunctions:          syncedFunctions,
		PrimarySharedInformers:   primarySharedInformers,
		SecondarySharedInformers: secondarySharedInformers,
	}
	c = &V3DepController{
		BaseController:     baseController,
		kubeClientset:      kubeClientset,
		ambClientset:       ambClientset,
		v3DepInformer:      v3DepInformer,
		v3DepLister:        v3DepLister,
		ingressInformer:    ingressInformer,
		ingressLister:      ingressLister,
		ingressTools:       ingressTools,
		hostInformer:       hostInformer,
		hostLister:         hostLister,
		hostTools:          hostTools,
		mappingInformer:    mappingInformer,
		mappingLister:      mappingLister,
		mappingTools:       mappingTools,
		kukuDomainInformer: kukuDomainInformer,
		kukuDomainLister:   kukuDomainLister,
		kukuDomainTools:    kukuDomainTools,
		kukuCertInformer:   kukuCertInformer,
		kukuCertLister:     kukuCertLister,
		kukuCertTools:      kukuCertTools,
		kukuCaInformer:     kukuCaInformer,
		kukuCaLister:       kukuCaLister,
		kukuCaTools:        kukuCaTools,
		kukuPortInformer:   kukuPortInformer,
		kukuPortLister:     kukuPortLister,
		kukuPortTools:      kukuPortTools,
		kukuLinkInformer:   kukuLinkInformer,
		kukuLinkLister:     kukuLinkLister,
		ambRecorder:        ambRecorder,
		controllerConfig:   controllerConfig,
	}
	// We use this "two-direction relationshipt" to implement abstract methods
	// See base/controller.go and this article:
	// https://medium.com/@adrianwit/abstract-class-reinvented-with-go-4a7326525034
	c.BaseController.IBaseController = c
	return
}

// Reconfig receives a new configuration for the controller: store it and
// reevaluate all primary objects
func (c *V3DepController) Reconfig(config common.ControllerConfig) {
	meth := c.Name + ".Reconfig()"
	c.controllerConfig = config
	namespace := ""
	matchLabels := map[string]string{}
	options := labels.SelectorFromSet(matchLabels)
	v3deps, err := c.v3DepLister.V3Deployments(namespace).List(options)
	if err != nil {
		log.Infof("%s. Cannot get V3Deployments to apply a config file change. Error: %s", meth, err.Error())
	}
	if v3deps != nil {
		for _, v3dep := range v3deps {
			log.Debugf("%s enqueue due to reconfig %s", meth, v3dep.GetName())
			c.EnqueueObject(v3dep)
		}
	}
}

// HandleSecondary is in charge of processing changes in the secondary objects
// (ingress, mappings, hosts) and force to re-evaluate the primary object
// (v3deployment)
// Note: ingress objects are used in ExternalDNS plugin, not in an Ingress plugin!
//
// There are special secondaries: kukudomain, kukucert, kukuca, kukuport. Its
// owner are not the v3dep, but we want it to be to be re-evaluated if the
// kukudomain/kukucert/kukuca/kukuport related to it is changed.
func (c *V3DepController) HandleSecondary(obj interface{}) {
	meth := c.Name + ".HandleSecondary()"
	log.Info(meth)

	object, objectName, err := c.GetObject(obj)
	if err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}
	log.Debugf("%s Processing object: %s", meth, object.GetName())

	switch object.(type) {

	//
	// Domains
	//
	case *kumoriv1.KukuDomain:
		log.Debugf("%s Processing Domain: %s", meth, object.GetName())
		kukuDomain := obj.(*kumoriv1.KukuDomain)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukudomain '%s'", meth, object.GetSelfLink(), object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, object.GetName(), err)
		}
		v3dep, err := c.kukuDomainTools.GetV3Deployment(kukuDomain)
		if errors.IsNotFound(err) || v3dep == nil {
			log.Infof("%s ignoring kukudomain '%s'. V3Deployment not found", meth, object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving V3Deployment for KukuDomain %s: %v", meth, object.GetName(), err)
			return
		}
		log.Debugf("%s enqueue %s", meth, v3dep.Name)
		c.EnqueueObject(v3dep)

	//
	// Certificates
	//
	case *kumoriv1.KukuCert:
		log.Debugf("%s Processing Cert: %s", meth, object.GetName())
		kukuCert := obj.(*kumoriv1.KukuCert)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukucert '%s'", meth, object.GetSelfLink(), object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, object.GetName(), err)
		}
		v3deps, err := c.kukuCertTools.GetV3Deployments(kukuCert)
		if errors.IsNotFound(err) || v3deps == nil || len(v3deps) == 0 {
			log.Infof("%s ignoring kukucert '%s'. V3Deployment not found", meth, object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving V3Deployment for KukuCert %s: %v", meth, object.GetName(), err)
			return
		}
		for _, v3dep := range v3deps {
			log.Debugf("%s enqueue %s", meth, v3dep.GetName())
			c.EnqueueObject(v3dep)
		}

	//
	// CAs
	//
	case *kumoriv1.KukuCa:
		log.Debugf("%s Processing Ca: %s", meth, object.GetName())
		kukuCa := obj.(*kumoriv1.KukuCa)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukuca '%s'", meth, object.GetSelfLink(), object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, object.GetName(), err)
		}
		v3deps, err := c.kukuCaTools.GetV3Deployments(kukuCa)
		if errors.IsNotFound(err) || v3deps == nil || len(v3deps) == 0 {
			log.Infof("%s ignoring kukuca '%s'. V3Deployment not found", meth, object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving V3Deployment for KukuCa %s: %v", meth, object.GetName(), err)
			return
		}
		for _, v3dep := range v3deps {
			log.Debugf("%s enqueue %s", meth, v3dep.GetName())
			c.EnqueueObject(v3dep)
		}

	//
	// Ports
	//
	case *kumoriv1.KukuPort:
		log.Debugf("%s Processing Port: %s", meth, object.GetName())
		kukuPort := obj.(*kumoriv1.KukuPort)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of kukuport '%s'", meth, object.GetSelfLink(), object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, object.GetName(), err)
		}
		v3dep, err := c.kukuPortTools.GetV3Deployment(kukuPort)
		if errors.IsNotFound(err) || v3dep == nil {
			log.Infof("%s ignoring kukuport '%s'. V3Deployment not found", meth, object.GetName())
			return
		} else if err != nil {
			log.Warnf("%s error retrieving V3Deployment for kukuport %s: %v", meth, object.GetName(), err)
			return
		}
		log.Debugf("%s enqueue %s", meth, v3dep.Name)
		c.EnqueueObject(v3dep)

	default:
		log.Debugf("%s Processing V3Deployment %s", meth, object.GetName())
		v3dep, err := c.v3DepLister.V3Deployments(object.GetNamespace()).Get(objectName)
		if errors.IsNotFound(err) {
			log.Infof("%s ignoring orphaned object '%s' of V3Deployment '%s'", meth, object.GetSelfLink(), objectName)
			return
		} else if err != nil {
			log.Warnf("%s error retrieving object %s: %v", meth, objectName, err)
		}
		log.Debugf("%s enqueue %s", meth, v3dep.GetName())
		c.EnqueueObject(v3dep)
	}
}

// SyncHandler compares the actual state with the desired, and attempts to
// converge the two.
func (c *V3DepController) SyncHandler(key string) error {
	meth := c.Name + ".SyncHandler()"
	log.Infof("%s %s", meth, key)

	// Errors have occurred that have not aborted the processing, but imply that
	// the primary object must be requeued at the end
	pendingRetryErrors := false

	// Retrieve v3Dep object
	v3Dep, err := c.getV3Deployment(key)
	abort, errToReturn := kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// Only inbound-builtin v3Deployments are taken into account
	if !inboundconfig.IsInbound(v3Dep, c.controllerConfig.InboundVersions) {
		return nil
	}

	// Check if v3Dep is being deleted (if it has a deletion timestamp set)
	if !v3Dep.GetDeletionTimestamp().IsZero() {
		log.Debugf("%s. V3Deployment has a deletion timestamp set. Skipping.", meth)
		return nil
	}

	// Get inbound configuration, from the v3dep configuration (params and resources)
	// If an error occur, the v3dep must be requeued
	inboundConfig, err := inboundconfig.NewInboundConfig(
		v3Dep,
		c.kukuDomainTools,
		c.kukuCertTools,
		c.kukuCaTools,
		c.kukuPortTools,
	)
	if err != nil {
		return err
	}
	log.Infof("%s. InboundConfig: %s", meth, inboundConfig.ToString())

	// If the inbound is a tcpinbound, this controller doesn't need to do nothing.
	// TcpInbound only implies actions when it is linked.
	if inboundConfig.IsTcpInbound() {
		log.Infof("%s V3Deployment %s is a TcpInbound: nothing to do", meth, v3Dep.GetName())
		return nil
	}

	// Do work related to Ambassador-Ingress plugin
	err = c.SyncHandlerAmbassador(v3Dep, inboundConfig.HttpInbound, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// Do work related to ExtenalDNS plugin
	err = c.SyncHandlerExternalDNS(v3Dep, inboundConfig.HttpInbound, &pendingRetryErrors)
	abort, errToReturn = kerrors.CheckErrorEx(err, &pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// If an error has occurred during the process (without abort it), we return
	// an error so the kukusolution is requeued
	if pendingRetryErrors {
		return fmt.Errorf("non-critical errors ocurred processing %s", key)
	}

	return nil
}

// getV3Deployment retrieve the v3dep object from its workqueue key
func (c *V3DepController) getV3Deployment(
	key string,
) (v3Dep *kumoriv1.V3Deployment, kerr error) {
	meth := c.Name + ".getV3Deployment()"

	namespace, name, err := cache.SplitMetaNamespaceKey(key)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("Invalid resource key: %s", key))
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	v3Dep, err = c.v3DepLister.V3Deployments(namespace).Get(name)
	if err != nil {
		if errors.IsNotFound(err) {
			kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("'%s' in work queue no longer exists", key))
			log.Warnf("%s %s", meth, kerr.Error())
		} else {
			kerr = kerrors.NewKukuError(kerrors.Retry, err)
			log.Errorf("%s %s", meth, kerr.Error())
		}
		return
	}
	return
}
