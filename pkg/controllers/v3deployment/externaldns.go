/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v3depcontroller

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	networkingv1 "k8s.io/api/networking/v1"
)

// SyncHandlerExternalDNS compares the actual state with the desired, and
// attempts to converge the two.
func (c *V3DepController) SyncHandlerExternalDNS(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	pendingRetryErrors *bool,
) error {
	meth := c.Name + ".SyncHandlerExternalDNS()"
	log.Infof(meth)

	// ---------------------------------------------------------------------------
	// For each secondary resource (kube-ingress) related to the primary
	// (v3dep), we get the list of items to removed/updated/created
	// ---------------------------------------------------------------------------

	ingressesToBeRemoved, ingressesToBeUpdated, ingressesToBeCreated, err :=
		c.getIngressChanges(v3Dep, httpInboundConfig, pendingRetryErrors)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	// ---------------------------------------------------------------------------
	// Log work to be done
	// ---------------------------------------------------------------------------

	if len(ingressesToBeRemoved) == 0 &&
		len(ingressesToBeUpdated) == 0 &&
		len(ingressesToBeCreated) == 0 {
		log.Infof("%s Nothing to do", meth)
	} else {
		msg := fmt.Sprintf(
			"ingress(%d,%d,%d)",
			len(ingressesToBeRemoved),
			len(ingressesToBeUpdated),
			len(ingressesToBeCreated),
		)
		log.Infof("%s Work to do (remove/update/create): %s", meth, msg)
	}

	// ---------------------------------------------------------------------------
	// For each secondary resource (kube-ingress) related to the primary
	// (kukuHttpInbound), we sync its items
	// ---------------------------------------------------------------------------

	err = c.syncIngress(
		ingressesToBeRemoved,
		ingressesToBeUpdated,
		ingressesToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return errToReturn
	}

	return nil
}

// getIngressChanges return the maps of ingresses to be removed, updated and created
func (c *V3DepController) getIngressChanges(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	pendingRetryErrors *bool,
) (
	ingressesToBeRemoved map[string]*networkingv1.Ingress,
	ingressesToBeUpdated map[string]*networkingv1.Ingress,
	ingressesToBeCreated map[string]*networkingv1.Ingress,
	errToReturn error,
) {
	ingressesToBeRemoved = make(map[string]*networkingv1.Ingress)
	ingressesToBeUpdated = make(map[string]*networkingv1.Ingress)
	ingressesToBeCreated = make(map[string]*networkingv1.Ingress)
	ingressesNotChanged := make(map[string]*networkingv1.Ingress)

	// Get the map of ingresses currently related to v3Dep (via owner label)
	currentIngresses, err := c.ingressTools.GetCurrentMap(v3Dep)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	// Get the list of ingresses to be updated/created (at most one, because
	// the relationship is one-to-one in this case)
	obj := c.ingressTools.GenerateIngress(
		v3Dep,
		httpInboundConfig,
		c.controllerConfig,
	)

	name := obj.GetName()
	if _, ok := currentIngresses[name]; !ok {
		ingressesToBeCreated[name] = obj
	} else if !c.ingressTools.Equal(obj, currentIngresses[name]) {
		ingressesToBeUpdated[name] = c.ingressTools.MergeToCurrent(currentIngresses[name], obj)
	} else {
		ingressesNotChanged[name] = obj
	}

	// Get the list of mapping to be removed (only possible if we are updating
	// the controller and, for example, the algorithm of the kubesecrets naming
	// has changed)
	for k, v := range currentIngresses {
		_, ok1 := ingressesToBeCreated[k]
		_, ok2 := ingressesToBeUpdated[k]
		_, ok3 := ingressesNotChanged[k]
		if ok1 == false && ok2 == false && ok3 == false {
			ingressesToBeRemoved[k] = v
		}
	}

	return
}

// syncIngress executes the provided actions (ingressesToBeRemoved, ingressesToBeUpdated,
// ingressesToBeCreated) to sync the state of ingress
func (c *V3DepController) syncIngress(
	ingressesToBeRemoved map[string]*networkingv1.Ingress,
	ingressesToBeUpdated map[string]*networkingv1.Ingress,
	ingressesToBeCreated map[string]*networkingv1.Ingress,
	v3Dep *kumoriv1.V3Deployment,
	pendingRetryErrors *bool,
) (errToReturn error) {
	meth := c.Name + ".syncMapping()"
	log.Infof(meth)

	err := c.ingressTools.ExecuteActionInMap(
		common.ActionDelete,
		ingressesToBeRemoved,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.ingressTools.ExecuteActionInMap(
		common.ActionUpdate,
		ingressesToBeUpdated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	err = c.ingressTools.ExecuteActionInMap(
		common.ActionCreate,
		ingressesToBeCreated,
		v3Dep,
		pendingRetryErrors,
	)
	abort, errToReturn = kerrors.CheckErrorEx(err, pendingRetryErrors)
	if abort {
		return
	}

	return
}
