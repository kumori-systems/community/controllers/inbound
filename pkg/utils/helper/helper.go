/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package helper

import (
	"fmt"
	"hash/crc32"
	common "httpinbound-controller/pkg/utils/common"
	naming "httpinbound-controller/pkg/utils/naming"
	"strings"

	log "github.com/sirupsen/logrus"
	labels "k8s.io/apimachinery/pkg/labels"
)

// createLabelSelector create a label selector for a resource
// Take into account that, currently:
//   - The "resource" string could contain just the name (without domain) of the resource
//     In that case, it is assumed that the domain and owner is the same as the
//     v3dep (and, in fact, domain=owner)
func CreateLabelSelector(
	v3depOwner, v3depDomain, resName string,
) (
	labels.Selector, error,
) {

	domain := ""
	name := ""
	parts := strings.Split(resName, "/")
	if len(parts) == 2 {
		domain = parts[0]
		name = parts[1]
	} else if len(parts) == 1 {
		domain = v3depDomain
		name = parts[0]
	} else {
		return nil, fmt.Errorf("invalid domain/name: %s", resName)
	}

	matchLabels := map[string]string{
		common.DomainLabel: naming.Hash(domain),
		common.NameLabel:   naming.Hash(name),
	}
	return labels.SelectorFromSet(matchLabels), nil
}

// UpdateLogLevel sets loglovel using configuration parameters
func UpdateLogLevel(logLevel string) {
	log.Infof("Setting log level to %s", logLevel)
	switch logLevel {
	case "debug":
		log.SetLevel(log.DebugLevel)
	case "info":
		log.SetLevel(log.InfoLevel)
	case "warn":
		log.SetLevel(log.WarnLevel)
	case "error":
		log.SetLevel(log.ErrorLevel)
	default:
		log.Warnf("Unknown log level %s. Ignored", logLevel)
	}
}

// CrcString calculates a crc32 from a string, and is returned as string
// As crc32 is used, the returned string has a length of 8 characters
// What about shorter crc? The crc package included in golang is always a
// crc32, the size cant be changed. We can use non-standar packages, but I
// dont like it because are not very used.
func CrcString(value string) string {

	// In this package, the CRC polynomial is represented in reversed notation,
	// or LSB-first representation.
	//
	// LSB-first representation is a hexadecimal number with n bits, in which the
	// most significant bit represents the coefficient of x⁰ and the least significant
	// bit represents the coefficient of xⁿ⁻¹ (the coefficient for xⁿ is implicit).
	//
	// For example, CRC32-Q, as defined by the following polynomial,
	//	x³²+ x³¹+ x²⁴+ x²²+ x¹⁶+ x¹⁴+ x⁸+ x⁷+ x⁵+ x³+ x¹+ x⁰
	// has the reversed notation 0b11010101100000101000001010000001, so the value
	// that should be passed to MakeTable is 0xD5828281.
	crc32q := crc32.MakeTable(0xD5828281)
	crc := crc32.Checksum([]byte(value), crc32q)
	crcStr := fmt.Sprintf("%x", crc)
	return crcStr
}

// Workaround to golang limitation: allow initialize pointers to basic types
// https://github.com/golang/go/issues/19966
// https://github.com/golang/go/issues/9097
func IntPtr(i int) *int       { return &i }
func StrPtr(s string) *string { return &s }
func BoolPtr(b bool) *bool    { return &b }
