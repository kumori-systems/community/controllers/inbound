/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package common

import (
	"strings"
	"time"
)

// Allowed actions for ExecuteActionInMap
const (
	ActionCreate = "create"
	ActionUpdate = "update"
	ActionDelete = "delete"
)

// Kind field in kubernetes manifests
const KindKukuCA = "KukuCa"
const KindKukuCert = "KukuCert"
const KindV3Deployment = "V3Deployment"
const KindKukuLink = "KukuLink"
const FieldManager = "kumori-kuinbound"

// Miscellaneous
const (
	OutOfServiceApp     = "outofservice"
	OutOfServicePortStr = "5000"
	KeprName            = "kumori-default"
	KeprServiceName     = "kumori-experimental"
)

// RequestTimeout is a function to "simulate" a time.Duration constant
func RequestTimeout() time.Duration {
	aux, _ := time.ParseDuration("10h")
	return aux
}

// IdleTimeout is a function to "simulate" a time.Duration constant
func IdleTimeout() time.Duration {
	aux, _ := time.ParseDuration("10h")
	return aux
}

// AmbassadorID returns the default AmbassadorID, required in Ambassador v3 CRDs
func AmbassadorID() []string {
	return []string{"default"}
}

// Message constants used with events
const (
	ReasonResourceCreated  = "Created"
	ReasonResourceUpdated  = "Updated"
	ReasonResourceDeleted  = "Deleted"
	MessageResourceCreated = "%s %s created"
	MessageResourceUpdated = "%s %s updated"
	MessageResourceDeleted = "%s %s deleted"
)

// Labels and annotations
const (
	OwnerLabel        = "kumori/owner"
	DomainLabel       = "kumori/domain"
	NameLabel         = "kumori/name"
	CommentAnnotation = "kumori/comment"
	ListenerType      = "kumori/listenertype"

	// Annotation used by ExternalDns plugin
	ExternalDnsAnnotation = "external-dns.alpha.kubernetes.io/target"
)

// ControllerConfig contains the kuinbound configuration
type ControllerConfig struct {
	IngressCNAME    string
	MinTLSVersion   string
	MaxTLSVersion   string
	CipherSuitesTLS []string
	EcdhCurvesTLS   []string
	HttpPort        int
	InboundVersions []string
}

// NewControllerConfig returns a new ControllerConfig
func NewControllerConfig(
	ingressCNAME string,
	minTLSVersion string,
	maxTLSVersion string,
	cipherSuitesTLSstr string,
	ecdhCurvesTLSstr string,
	httpPort int,
	inboundVersionsStr string,
) ControllerConfig {
	return ControllerConfig{
		IngressCNAME:    ingressCNAME,
		MinTLSVersion:   minTLSVersion,
		MaxTLSVersion:   maxTLSVersion,
		CipherSuitesTLS: strings.Split(cipherSuitesTLSstr, ","),
		EcdhCurvesTLS:   strings.Split(ecdhCurvesTLSstr, ","),
		HttpPort:        httpPort,
		InboundVersions: strings.Split(inboundVersionsStr, ","),
	}
}
