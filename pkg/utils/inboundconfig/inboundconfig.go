/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package inboundconfig

import (
	"encoding/json"
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kukucatools "httpinbound-controller/pkg/utils/kuku/kukuca"
	kukucerttools "httpinbound-controller/pkg/utils/kuku/kukucert"
	kukudomaintools "httpinbound-controller/pkg/utils/kuku/kukudomain"
	kukuporttools "httpinbound-controller/pkg/utils/kuku/kukuport"
	"strconv"
	"strings"

	semver "github.com/Masterminds/semver/v3"
	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	runtime "k8s.io/apimachinery/pkg/runtime"
)

// IsInbound return true only if the v3deployment is an inbound-builtin service
func IsInbound(v3Dep *kumoriv1.V3Deployment, InboundVersions []string) bool {
	meth := "inboundconfig.IsInbound"

	if v3Dep.Spec.Artifact == nil ||
		v3Dep.Spec.Artifact.Description == nil ||
		!v3Dep.Spec.Artifact.Description.Builtin {
		return false
	}

	reference := v3Dep.Spec.Artifact.Ref.ToString()
	log.Debugf("%s Checking builtin reference %s", meth, reference)
	for _, constraint := range InboundVersions {
		matches, err := checkReference(reference, constraint)
		if err != nil {
			log.Warnf("%s Error checking reference: %s (reference:%s, contraint:%s)",
				meth, err.Error(), reference, constraint)
		}
		if matches {
			log.Debugf("%s Inbound %s is supported", meth, reference)
			return true
		}
	}

	// Because currently the inbound is the only one builtin service, we warn
	// all the kuinbound-not-supported builtin services.
	log.Warnf("%s Inbound %s is not supported", meth, reference)
	return false
}

type AccessPolicies struct {
	AllowedIpList *[]string `json:"allowediplist,omitempty"`
	DeniedIpList  *[]string `json:"deniediplist,omitempty"`
}

type InboundConfig struct {
	InboundType string // "https" or "tcp"
	HttpInbound *HttpInboundConfig
	TcpInbound  *TcpInboundConfig
}

type HttpInboundConfig struct {
	ID                  string // The internal name of the deployed http inbound
	ClientCert          bool
	CertRequired        bool // Only if ClientCert=true
	Websocket           bool
	CleanXForwardedFor  bool
	RemoteAddressHeader string
	AccessPolicies      *AccessPolicies
	Domain              kumoriv1.KukuDomain
	Certificate         kumoriv1.KukuCert
	CA                  *kumoriv1.KukuCa
}

type TcpInboundConfig struct {
	ID   string // The iternal name of the deployed tcp inbound
	Port kumoriv1.KukuPort
}

func (i InboundConfig) IsTcpInbound() bool {
	return (i.InboundType == "tcp")
}

func (i InboundConfig) IsHttpInbound() bool {
	return (i.InboundType == "https")
}

func (i InboundConfig) ToString() string {
	text := i.InboundType
	if i.HttpInbound != nil {
		text = text +
			", ID: " + i.HttpInbound.ID +
			", ClientCert: " + strconv.FormatBool(i.HttpInbound.ClientCert) +
			", CertRequired: " + strconv.FormatBool(i.HttpInbound.CertRequired) +
			", Websocket: " + strconv.FormatBool(i.HttpInbound.Websocket) +
			", CleanXForwardedFor: " + strconv.FormatBool(i.HttpInbound.CleanXForwardedFor) +
			", RemoteAddressHeader: " + i.HttpInbound.RemoteAddressHeader +
			", Domain: " + i.HttpInbound.Domain.Spec.Domain +
			", CertDomain: " + i.HttpInbound.Certificate.Data.Domain
		if i.HttpInbound.CA != nil {
			text = text + ", CA: <value>"
		}
	} else if i.TcpInbound != nil {
		text = text + ", ID: " + i.TcpInbound.ID + ", Port: " + strconv.Itoa(i.TcpInbound.Port.Spec.Port)
	}
	return text
}

func NewInboundConfig(
	v3Dep *kumoriv1.V3Deployment,
	kukuDomainTools kukudomaintools.KukuDomainTools,
	kukuCertTools kukucerttools.KukuCertTools,
	kukuCaTools kukucatools.KukuCaTools,
	kukuPortTools kukuporttools.KukuPortTools,
) (
	inboundConfig *InboundConfig, err error,
) {
	meth := "inboundconfig.NewInboundConfig"
	v3DepParameterMap := v3Dep.Spec.Artifact.Description.Config.Parameters
	v3DepResourceMap := *v3Dep.Spec.Artifact.Description.Config.Resource
	v3depOwner := v3Dep.GetAnnotations()[common.OwnerLabel]
	v3depDomain := v3Dep.GetAnnotations()[common.DomainLabel]
	v3depName := v3Dep.GetName()
	v3depNamespace := v3Dep.GetNamespace()

	// Read all parameters
	inboundType, clientCert, certRequired, webSocket, cleanXForwardedFor,
		remoteAddressHeader, accessPolicies, err := rawToValues(v3DepParameterMap)
	if err != nil {
		err = fmt.Errorf("error creating inbound configuration: %s", err.Error())
		log.Errorf("%s Error %v", meth, err)
		return nil, err
	}

	if inboundType == "https" {

		// Retrieve http inbound resources

		// Domain
		domainResName := v3DepResourceMap["serverdomain"].Domain
		if domainResName == nil {
			err = fmt.Errorf("domain resource is null")
			log.Warnf("%s %v", meth, err)
			return nil, err
		}
		domain, err := kukuDomainTools.GetDomainByOwnerAndName(
			v3depNamespace, v3depOwner, v3depDomain, *domainResName,
		)
		if err != nil {
			err = fmt.Errorf("error searching domain resource: %s", err.Error())
			log.Warnf("%s %v", meth, err)
			return nil, err
		}

		// Certificate
		certResName := v3DepResourceMap["servercert"].Certificate
		if certResName == nil {
			err = fmt.Errorf("certificate resource is null")
			log.Warnf("%s %v", meth, err)
			return nil, err
		}
		cert, err := kukuCertTools.GetCertByOwnerAndName(
			v3depNamespace, v3depOwner, v3depDomain, *certResName,
		)
		if err != nil {
			err = fmt.Errorf("error searching certificate resource: %s", err.Error())
			log.Warnf("%s %v", meth, err)
			return nil, err
		}

		caResName := v3DepResourceMap["clientcertca"].CA
		var ca *kumoriv1.KukuCa = nil
		if caResName != nil {
			ca, err = kukuCaTools.GetCAByOwnerAndName(
				v3depNamespace, v3depOwner, v3depDomain, *caResName,
			)
			if err != nil {
				err = fmt.Errorf("error searching ca resource: %s", err.Error())
				log.Warnf("%s %v", meth, err)
				return nil, err
			}
		}

		inboundConfig = &InboundConfig{
			InboundType: inboundType,
			HttpInbound: &HttpInboundConfig{
				ID:                  v3depName,
				ClientCert:          clientCert,
				CertRequired:        certRequired,
				Websocket:           webSocket,
				CleanXForwardedFor:  cleanXForwardedFor,
				RemoteAddressHeader: remoteAddressHeader,
				AccessPolicies:      accessPolicies,
				Domain:              *domain,
				Certificate:         *cert,
				CA:                  ca,
			},
			TcpInbound: nil,
		}
	} else if inboundType == "tcp" {

		// Retrieve http inbound resources
		portResName := *v3DepResourceMap["port"].Port
		port, err := kukuPortTools.GetPortByOwnerAndName(
			v3depNamespace, v3depOwner, v3depDomain, portResName,
		)
		if err != nil {
			err = fmt.Errorf("error searching port resource: %s", err.Error())
			log.Warnf("%s %v", meth, err)
			return nil, err
		}

		inboundConfig = &InboundConfig{
			InboundType: inboundType,
			HttpInbound: nil,
			TcpInbound: &TcpInboundConfig{
				ID:   v3depName,
				Port: *port,
			},
		}
	} else {
		err = fmt.Errorf("error creating inbound configuration: invalid inbound config type %s", inboundType)
		log.Errorf("%s Error %v", meth, err)
	}
	return
}

func rawToValues(
	rawConfig *map[string]runtime.RawExtension,
) (
	inboundType string,
	clientCert bool,
	certRequired bool,
	webSocket bool,
	cleanXForwardedFor bool,
	remoteAddressHeader string,
	accessPolicies *AccessPolicies,
	err error,
) {
	meth := "inboundconfig.rawToValues"
	inboundTypeBytes := (*rawConfig)["type"].Raw
	if err = json.Unmarshal(inboundTypeBytes, &inboundType); err != nil {
		log.Errorf("%s Error %v", meth, err)
		return
	}
	clientCertBytes := (*rawConfig)["clientcert"].Raw
	if len(clientCertBytes) > 0 {
		if err = json.Unmarshal(clientCertBytes, &clientCert); err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}
	}
	certRequired = false
	if clientCert {
		certRequiredBytes := (*rawConfig)["certrequired"].Raw
		if len(certRequiredBytes) > 0 {
			if err = json.Unmarshal(certRequiredBytes, &certRequired); err != nil {
				log.Errorf("%s Error %v", meth, err)
				return
			}
		}
	}
	webSocketBytes := (*rawConfig)["websocket"].Raw
	if len(webSocketBytes) > 0 {
		if err = json.Unmarshal(webSocketBytes, &webSocket); err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}
	}
	cleanXForwardedForBytes := (*rawConfig)["cleanxforwardedfor"].Raw
	if len(cleanXForwardedForBytes) > 0 {
		if err = json.Unmarshal(cleanXForwardedForBytes, &cleanXForwardedFor); err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}
	}
	remoteAddressHeaderBytes := (*rawConfig)["remoteaddressheader"].Raw
	if len(remoteAddressHeaderBytes) > 0 {
		if err = json.Unmarshal(remoteAddressHeaderBytes, &remoteAddressHeader); err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}
	}
	accessPoliciesBytes := (*rawConfig)["accesspolicies"].Raw
	if len(accessPoliciesBytes) > 0 {
		accessPolicies = &AccessPolicies{}
		if err = json.Unmarshal(accessPoliciesBytes, accessPolicies); err != nil {
			log.Errorf("%s Error %v", meth, err)
			return
		}
	}
	return
}

// checkReference checks if a builtin/inbound reference ...
//   - domain[/module]/name/@version[-prerelease][+.buildmetadata]:kind
//
// ... matches a valid reference constraint, taking into account semver sintax.
// For semver sintax, "*", "~" and "^" symbols are allowed to specify ranges
// (see https://github.com/Masterminds/semver)
// For example:
//   - "kumori.systems/builtins/inbound/@1.0.0:service" matches
//     "kumori.systems/builtins/inbound/@~1:service"
//   - "kumori.systems/builtins/inbound/inbound/@1.1.0:service" matches
//     "kumori.systems/builtins/inbound/inbound/@1.1.*:service"
//   - "kumori.systems/builtins/inbound/inbound/@1.1.0:service" matches
//     "kumori.systems/builtins/inbound/inbound/@1.1:service"
func checkReference(reference, constraint string) (bool, error) {

	refName, refVersion, refKind, err := splitReference(reference)
	if err != nil {
		return false, err
	}
	refSemver, err := semver.NewVersion(refVersion)
	if err != nil {
		return false, err
	}

	constName, constVersion, constKind, err := splitReference(constraint)
	if err != nil {
		return false, err
	}
	constSemver, err := semver.NewConstraint(constVersion)
	if err != nil {
		return false, err
	}

	if refName != constName || refKind != constKind || !constSemver.Check(refSemver) {
		return false, nil
	}

	return true, nil
}

// splitReference splits a reference string...
//   - domain[/module]/name/@version[-prerelease][+.buildmetadata]:kind
//
// ... into its parts:
//   - name = "domain[/module]/name"
//   - version = "version[-prerelease][+.buildmetadata]"
//   - kind = "kind"
func splitReference(ref string) (name, version, kind string, err error) {

	aux := strings.Split(ref, "/@")
	if len(aux) != 2 {
		err = fmt.Errorf("invalid reference format %s", ref)
	}
	name = aux[0]
	rest := aux[1]

	aux = strings.Split(rest, ":")
	if len(aux) == 1 {
		// This case is related to older uses of builtin/inbound (backwards compatibility)
		version = aux[0]
		kind = ""
	} else if len(aux) == 2 {
		version = aux[0]
		kind = aux[1]
	} else {
		err = fmt.Errorf("invalid reference format %s", ref)
	}
	return
}
