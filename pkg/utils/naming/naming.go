/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
	"hash/fnv"
	"strconv"
)

func CalculateUnlinkMappingName(ownerName string) string {
	return ownerName + "-mapping-unlinked"
}

func CalculateLinkMappingName(ownerName string, serviceName string) string {
	return ownerName + "." + serviceName + ".mapping-linked"
}

func CalculateLinkTCPMappingName(ownerName string, tcpport int) string {
	return ownerName + "-" + strconv.Itoa(tcpport) + "-tcpmapping-linked"
}

func CalculateIngressName(kukuhttpinboundName string) string {
	return kukuhttpinboundName + "-extdns-ingress"
}

func CalculateHostName(ownerName string) string {
	return ownerName + "-host"
}

func CalculateTLSContextName(ownerName string) string {
	return ownerName + "-tlscontext"
}

func CalculateCertSecretName(kukucertName string) string {
	return kukucertName + "-cert"
}

func CalculateCASecretName(kukucaName string) string {
	return kukucaName + "-ca"
}

func CalculateKubeServiceName(kukuDeploymentName, connectorName string, tag string) string {
	baseName := kukuDeploymentName + "-" + Hash(connectorName+"-"+tag)
	return baseName + "-lb-service"
}

// Hash returns a hashed version of a given string
func Hash(source string) string {
	hf := fnv.New32()
	hf.Write([]byte(source))
	return string(fmt.Sprintf("%08x", hf.Sum32()))
	// return rand.SafeEncodeString(fmt.Sprint(hf.Sum32()))
}
