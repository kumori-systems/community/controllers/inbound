/*
* Copyright 2022 Kumori Systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they will be approved by the European Commission - subsequent versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in writing, software distributed under the Licence is distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the Licence for the specific language governing permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
)

const (
	// Abort error means that the Kuku-Element processing cannot be ended
	// and cannot be requeued
	Abort = "abort"
	// Retry error means that the Kuku-Element processing cannot be ended
	// but can be requeued
	Retry = "retry"
	// Continue error means that the Kuku-Element processing can continue but
	// must be requeued because there might be some pieces missing
	Continue = "continue"
)

// Text related to a KukuError created from an invalid parent
const (
	InvalidSource = "Error constructed with invalid parent"
)

// KukuErrorSeverity is the severity of a KukuError
type KukuErrorSeverity string

// KukuError is an error produced while a Kuku was created. Message represents
// the error and Severity the impact on the Kuku-Elemen processing.
type KukuError struct {
	Parent   error
	Severity KukuErrorSeverity
}

// Error message returns a string indicating the error severity and the parent
// error message.
func (e *KukuError) Error() string {
	return fmt.Sprintf("Severity: %s. Parent: %s", e.Severity, e.Parent.Error())
}

// NewKukuError returns a new KukuError object
func NewKukuError(severity KukuErrorSeverity, source interface{}) *KukuError {
	var parent error
	switch source.(type) {
	case error:
		parent = source.(error)
	case string:
		parent = fmt.Errorf(source.(string))
	default:
		parent = fmt.Errorf(InvalidSource)
	}
	return &KukuError{Severity: severity, Parent: parent}
}

// CheckErrorEx analyze the error, like CheckError, but in more suitable
// format for the controller flow.
//   - Returns true, nil => when the processing must be stopped and not enqueued
//     (so the error is not returned)
//   - Returns true, err => when the processing must be stopped and enqueued (so
//     the error is returned)
//   - Returns false, nil => when the processing must not be stopped. But, if the
//     kuku-element must be requeued, the "reenqueue" provided boolean is updated.
func CheckErrorEx(err error, reenqueue *bool) (bool, error) {
	stop, retry := CheckError(err)
	if retry {
		*reenqueue = retry
	}
	if stop {
		if retry {
			return true, err
		}
		return true, nil
	}
	return false, nil
}

// CheckError analyzes the error and:
// - Returns a boolean meaning if current processing must be stopped or not.
//   Will be true when "Abort" and "Retry" errors
// - Returns a boolean meaning if the kuku-element must be requeued so it will
//   be reevaluated later (and maybe the error will not occur)
//   Will be true when "Retry" and "Continue" errors
//   Will be true when "Abort" and "Retry" errors

func CheckError(err error) (stop bool, retry bool) {
	stop = false
	retry = false
	if err == nil {
		return
	}
	switch err.(type) {
	case nil: // If is not a kukuerror, is handle as a kukuerror.Continue.
		stop = false
		retry = true
	case *KukuError:
		kerr := err.(*KukuError)
		switch kerr.Severity {
		case Abort:
			stop = true
			retry = false
		case Retry:
			stop = true
			retry = true
		case Continue:
			stop = false
			retry = true
		}
	}
	return
}
