/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
	// 	"strings"
	helper "httpinbound-controller/pkg/utils/helper"
	// kerrors "httpinbound-controller/pkg/utils/errors"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// KukuLinkTools includes functions related to kukulinks
type KukuDomainTools struct {
	kukuDomainLister kumorilisters.KukuDomainLister
	v3DepLister      kumorilisters.V3DeploymentLister
	kukuLinkLister   kumorilisters.KukuLinkLister
}

// NewKukuLinkTools returns a new KukuLinkTools
func NewKukuDomainTools(
	kukuDomainLister kumorilisters.KukuDomainLister,
	v3DepLister kumorilisters.V3DeploymentLister,
	kukuLinkLister kumorilisters.KukuLinkLister,
) (c KukuDomainTools) {
	c = KukuDomainTools{
		kukuDomainLister: kukuDomainLister,
		v3DepLister:      v3DepLister,
		kukuLinkLister:   kukuLinkLister,
	}
	return
}

// GetV3Deployment returns the V3Deployment that uses a KukuDomain
func (k *KukuDomainTools) GetV3Deployment(
	kukuDomain *kumoriv1.KukuDomain,
) (
	v3Deployment *kumoriv1.V3Deployment,
	err error,
) {
	name := kukuDomain.GetName()
	ns := kukuDomain.GetNamespace()
	matchLabels := map[string]string{
		name: "resource-in-use",
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	v3deps, err := k.v3DepLister.V3Deployments(ns).List(labelSelector)
	if errors.IsNotFound(err) || v3deps == nil || len(v3deps) <= 0 {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	if len(v3deps) > 1 {
		return nil, fmt.Errorf("domain %s is used by %d v3deployments", name, len(v3deps))
	}
	return v3deps[0], nil
}

// GetDomainByOwnerAndName returns the kukudomain with the owner and name provided.
// Returns an error if it is not found or any error occur.
func (k *KukuDomainTools) GetDomainByOwnerAndName(
	v3depNamespace, v3depOwner, v3depDomain, resName string,
) (
	*kumoriv1.KukuDomain, error,
) {
	labelSelector, err := helper.CreateLabelSelector(v3depOwner, v3depOwner, resName)
	if err != nil {
		return nil, err
	}
	kukuDomains, err := k.kukuDomainLister.KukuDomains(v3depNamespace).List(labelSelector)
	if errors.IsNotFound(err) || kukuDomains == nil || len(kukuDomains) <= 0 {
		return nil, fmt.Errorf("kukudomain not found")
	} else if err != nil {
		return nil, err
	}
	if len(kukuDomains) > 1 {
		return nil, fmt.Errorf("several (%d) kukudomains found", len(kukuDomains))
	}
	return kukuDomains[0], nil
}

// GetLink returns the kukuLink that uses a KukuDomain
func (k *KukuDomainTools) GetLink(kukuDomain *kumoriv1.KukuDomain) (
	kukuLink *kumoriv1.KukuLink,
	err error,
) {
	v3Dep, err := k.GetV3Deployment(kukuDomain)
	if err != nil {
		return nil, err
	}

	if v3Dep == nil {
		return nil, fmt.Errorf("the KukuDomain %s is not assigned to any V3Deployment", kukuDomain.Name)
	}

	kukuLink, err = k.GetLinkByDeploymentField(v3Dep, "deployment1")
	if kukuLink == nil && err == nil {
		kukuLink, err = k.GetLinkByDeploymentField(v3Dep, "deployment2")
	}
	return
}

func (k *KukuDomainTools) GetLinkByDeploymentField(
	v3Dep *kumoriv1.V3Deployment, deploymentField string,
) (
	kukuLink *kumoriv1.KukuLink, err error,
) {
	kukuLink = nil
	err = nil

	name := v3Dep.GetName()
	ns := v3Dep.GetNamespace()

	matchLabels := map[string]string{deploymentField: name}
	labelSelector := labels.SelectorFromSet(matchLabels)
	list, err := k.kukuLinkLister.KukuLinks(ns).List(labelSelector)
	if err != nil {
		return
	}
	if len(list) == 1 {
		kukuLink = list[0]
		return
	} else if len(list) > 1 {
		err = fmt.Errorf("there are several KukuLinks related to %s", name)
		return
	}

	return
}
