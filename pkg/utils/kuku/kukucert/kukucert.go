/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
	helper "httpinbound-controller/pkg/utils/helper"

	// kerrors "httpinbound-controller/pkg/utils/errors"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// KukuCertTools definition
type KukuCertTools struct {
	kukuCertLister kumorilisters.KukuCertLister
	v3DepLister    kumorilisters.V3DeploymentLister
}

// NewKukuCertTools returns a new KukuCertTools
func NewKukuCertTools(
	kukuCertLister kumorilisters.KukuCertLister,
	v3DepLister kumorilisters.V3DeploymentLister,
) (c KukuCertTools) {
	c = KukuCertTools{
		kukuCertLister: kukuCertLister,
		v3DepLister:    v3DepLister,
	}
	return
}

// GetV3Deployments returns the list of V3Deployments that uses a KukuCert
func (k *KukuCertTools) GetV3Deployments(
	kukuCert *kumoriv1.KukuCert,
) (
	v3Deployment []*kumoriv1.V3Deployment,
	err error,
) {
	name := kukuCert.GetName()
	ns := kukuCert.GetNamespace()
	matchLabels := map[string]string{
		name: "resource-in-use",
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	v3deps, err := k.v3DepLister.V3Deployments(ns).List(labelSelector)
	if errors.IsNotFound(err) || v3deps == nil || len(v3deps) <= 0 {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return v3deps, nil
}

// GetCertByOwnerAndName returns the kukucert with the owner and name provided.
// Returns an error if it is not found or any error occur.
func (k *KukuCertTools) GetCertByOwnerAndName(
	v3depNamespace, v3depOwner, v3depDomain, resName string,
) (
	*kumoriv1.KukuCert, error,
) {
	labelSelector, err := helper.CreateLabelSelector(v3depOwner, v3depOwner, resName)
	if err != nil {
		return nil, err
	}
	kukuCerts, err := k.kukuCertLister.KukuCerts(v3depNamespace).List(labelSelector)
	if errors.IsNotFound(err) || kukuCerts == nil || len(kukuCerts) <= 0 {
		return nil, fmt.Errorf("kukucert not found")
	} else if err != nil {
		return nil, err
	}
	if len(kukuCerts) > 1 {
		return nil, fmt.Errorf("several (%d) kukucerts found", len(kukuCerts))
	}
	return kukuCerts[0], nil
}
