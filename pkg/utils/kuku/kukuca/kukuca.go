/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (

	// kerrors "httpinbound-controller/pkg/utils/errors"
	// "strings"
	"fmt"
	helper "httpinbound-controller/pkg/utils/helper"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// KukuCaTools definition
type KukuCaTools struct {
	kukuCaLister kumorilisters.KukuCaLister
	v3DepLister  kumorilisters.V3DeploymentLister
}

// NewKukuCaTools returns a new KukuCaTools
func NewKukuCaTools(
	kukuCaLister kumorilisters.KukuCaLister,
	v3DepLister kumorilisters.V3DeploymentLister,
) (c KukuCaTools) {
	c = KukuCaTools{
		kukuCaLister: kukuCaLister,
		v3DepLister:  v3DepLister,
	}
	return
}

// GetV3Deployment returns the list of V3Deployments that uses a KukuCa
func (k *KukuCaTools) GetV3Deployments(
	kukuCa *kumoriv1.KukuCa,
) (
	v3Deployment []*kumoriv1.V3Deployment,
	err error,
) {
	name := kukuCa.GetName()
	ns := kukuCa.GetNamespace()
	matchLabels := map[string]string{
		name: "resource-in-use",
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	v3deps, err := k.v3DepLister.V3Deployments(ns).List(labelSelector)
	if errors.IsNotFound(err) || v3deps == nil || len(v3deps) <= 0 {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return v3deps, nil
}

// GetCAByOwnerAndName returns the kukuca with the owner and name provided.
// Returns an error if it is not found or any error occur.
func (k *KukuCaTools) GetCAByOwnerAndName(
	v3depNamespace, v3depOwner, v3depDomain, resName string,
) (
	*kumoriv1.KukuCa, error,
) {
	labelSelector, err := helper.CreateLabelSelector(v3depOwner, v3depOwner, resName)
	if err != nil {
		return nil, err
	}
	kukuCas, err := k.kukuCaLister.KukuCas(v3depNamespace).List(labelSelector)
	if errors.IsNotFound(err) || kukuCas == nil || len(kukuCas) <= 0 {
		return nil, fmt.Errorf("kukuca not found")
	} else if err != nil {
		return nil, err
	}
	if len(kukuCas) > 1 {
		return nil, fmt.Errorf("several (%d) kukucas found", len(kukuCas))
	}
	return kukuCas[0], nil
}
