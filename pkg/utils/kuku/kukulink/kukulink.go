/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	naming "httpinbound-controller/pkg/utils/naming"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	labels "k8s.io/apimachinery/pkg/labels"
)

// KukuLinkTools includes functions related to kukulinks
type KukuLinkTools struct {
	v3DeploymentLister kumorilisters.V3DeploymentLister
}

// NewKukuLinkTools returns a new KukuLinkTools
func NewKukuLinkTools(
	v3DeploymentLister kumorilisters.V3DeploymentLister,
) (c KukuLinkTools) {
	c = KukuLinkTools{
		v3DeploymentLister: v3DeploymentLister,
	}
	return
}

// GetLinkDeployments returns the v3deps and channels related to the kukulink
func (k *KukuLinkTools) GetLinkDeployments(kukuLink *kumoriv1.KukuLink) (
	v3Dep1 *kumoriv1.V3Deployment, ch1 string,
	v3Dep2 *kumoriv1.V3Deployment, ch2 string,
	err error,
) {
	ns := kukuLink.GetNamespace()
	owner := kukuLink.GetObjectMeta().GetAnnotations()[common.OwnerLabel]
	endPoint1 := kukuLink.Spec.Endpoints[0]
	endPoint2 := kukuLink.Spec.Endpoints[1]
	v3Dep1, ch1, err = k.getLinkDeployment(owner, ns, endPoint1)
	if err != nil {
		return
	}
	v3Dep2, ch2, err = k.getLinkDeployment(owner, ns, endPoint2)
	if err != nil {
		return
	}
	return
}

// getLinkDeployment returns the v3Deployment (and its channel) related to one
// part of kukulink
func (k *KukuLinkTools) getLinkDeployment(
	linkOwner, linkNamespace string, endpoint kumoriv1.KukuEndpoint,
) (
	v3Deployment *kumoriv1.V3Deployment,
	channel string,
	kerr error,
) {

	// Removed `kumori/owner` label from selector to allow links between deployments
	// from other owners (see ticket 1415)
	// matchLabels := map[string]string{
	// 	common.OwnerLabel:  naming.Hash(linkOwner),
	// 	common.DomainLabel: naming.Hash(endpoint.Domain),
	// 	common.NameLabel:   naming.Hash(endpoint.Name),
	// }
	matchLabels := map[string]string{
		common.DomainLabel: naming.Hash(endpoint.Domain),
		common.NameLabel:   naming.Hash(endpoint.Name),
	}
	labelSelector := labels.SelectorFromSet(matchLabels)

	v3DepStr := linkOwner + ":" + endpoint.Domain + "/" + endpoint.Name
	list, err := k.v3DeploymentLister.V3Deployments(linkNamespace).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
	} else if len(list) == 0 {
		kerr = kerrors.NewKukuError(kerrors.Retry, fmt.Sprintf("Deployment "+v3DepStr+" not found"))
	} else if len(list) > 1 {
		kerr = kerrors.NewKukuError(kerrors.Abort, fmt.Sprintf("There are several deployments "+v3DepStr))
	} else {
		v3Deployment = list[0]
		channel = endpoint.Channel
	}

	return
}

// LinkToString creates a string using kukulink info (logging) purposes)
func (k *KukuLinkTools) LinkToString(kukuLink *kumoriv1.KukuLink) string {
	ep1 := kukuLink.Spec.Endpoints[0]
	ep2 := kukuLink.Spec.Endpoints[1]
	return kukuLink.Name + ":" +
		ep1.Kind + "/" + ep1.Domain + "/" + ep1.Name + ":" + ep1.Channel +
		"<=>" +
		ep2.Kind + "/" + ep2.Domain + "/" + ep2.Name + ":" + ep2.Channel
}
