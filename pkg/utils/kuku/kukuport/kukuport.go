/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package utils

import (
	// 	"strings"
	// kerrors "httpinbound-controller/pkg/utils/errors"
	"fmt"

	helper "httpinbound-controller/pkg/utils/helper"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
)

// KukuPortTools includes functions related to kukuports
type KukuPortTools struct {
	kukuPortLister kumorilisters.KukuPortLister
	v3DepLister    kumorilisters.V3DeploymentLister
}

// NewKukuPortTools returns a new KukuPortTools
func NewKukuPortTools(
	kukuPortLister kumorilisters.KukuPortLister,
	v3DepLister kumorilisters.V3DeploymentLister,
) (c KukuPortTools) {
	c = KukuPortTools{
		kukuPortLister: kukuPortLister,
		v3DepLister:    v3DepLister,
	}
	return
}

// GetV3Deployment returns the V3Deployment that uses a KukuPort
func (k *KukuPortTools) GetV3Deployment(
	kukuPort *kumoriv1.KukuPort,
) (
	v3Deployment *kumoriv1.V3Deployment,
	err error,
) {
	name := kukuPort.GetName()
	ns := kukuPort.GetNamespace()
	matchLabels := map[string]string{
		name: "resource-in-use",
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	v3deps, err := k.v3DepLister.V3Deployments(ns).List(labelSelector)
	if errors.IsNotFound(err) || v3deps == nil || len(v3deps) <= 0 {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	if len(v3deps) > 1 {
		return nil, fmt.Errorf("port %s is used by %d v3deployments", name, len(v3deps))
	}
	return v3deps[0], nil
}

// GetPortByOwnerAndName returns the kukuport with the owner and name provided.
// Returns an error if it is not found or any error occur.
func (k *KukuPortTools) GetPortByOwnerAndName(
	v3depNamespace, v3depOwner, v3depDomain, resName string,
) (
	*kumoriv1.KukuPort, error,
) {
	labelSelector, err := helper.CreateLabelSelector(v3depOwner, v3depOwner, resName)
	if err != nil {
		return nil, err
	}
	kukuPorts, err := k.kukuPortLister.KukuPorts(v3depNamespace).List(labelSelector)
	if errors.IsNotFound(err) || kukuPorts == nil || len(kukuPorts) <= 0 {
		return nil, fmt.Errorf("kukuport not found")
	} else if err != nil {
		return nil, err
	}
	if len(kukuPorts) > 1 {
		return nil, fmt.Errorf("several (%d) kukuports found", len(kukuPorts))
	}
	return kukuPorts[0], nil
}
