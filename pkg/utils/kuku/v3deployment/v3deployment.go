/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package v3deployment

import (
	"fmt"

	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	kumorilisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/listers/kumori/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	labels "k8s.io/apimachinery/pkg/labels"
)

// V3DeploymentTools
type V3DeploymentTools struct {
	kukuV3DeploymentLister kumorilisters.V3DeploymentLister
	kukuLinkLister         kumorilisters.KukuLinkLister
}

// NewV3DeploymentTools returns a new V3DeploymentTools
func NewV3DeploymentTools(
	kukuV3DeploymentLister kumorilisters.V3DeploymentLister,
	kukuLinkLister kumorilisters.KukuLinkLister,
) (
	t V3DeploymentTools,
) {
	t = V3DeploymentTools{
		kukuV3DeploymentLister: kukuV3DeploymentLister,
		kukuLinkLister:         kukuLinkLister,
	}
	return
}

// GetV3DeploymentByPort returns the v3Deployment using the kukuport.
func (k *V3DeploymentTools) GetV3DeploymentByPort(
	kukuPort *kumoriv1.KukuPort,
) (
	*kumoriv1.V3Deployment, error,
) {
	name := kukuPort.GetName()
	ns := kukuPort.GetNamespace()
	matchLabels := map[string]string{
		name: "resource-in-use",
	}
	labelSelector := labels.SelectorFromSet(matchLabels)
	list, err := k.kukuV3DeploymentLister.V3Deployments(ns).List(labelSelector)
	if errors.IsNotFound(err) || list == nil || len(list) <= 0 {
		return nil, nil
	} else if err != nil {
		return nil, err
	} else if len(list) == 1 {
		return list[0], nil
	} else if len(list) > 1 {
		err = fmt.Errorf("there are several KukuPorts related to %s", name)
		return nil, err
	}
	return nil, nil
}

// GetLink returns the  kukuLink  using the v3dep
func (k *V3DeploymentTools) GetLinks(v3Dep *kumoriv1.V3Deployment) (
	kukuLinks []*kumoriv1.KukuLink,
	err error,
) {
	kukuLinks, err = k.GetLinksByDeploymentField(v3Dep, "deployment1")
	if len(kukuLinks) == 0 && err == nil {
		kukuLinks, err = k.GetLinksByDeploymentField(v3Dep, "deployment2")
	}
	return
}

func (k *V3DeploymentTools) GetLinksByDeploymentField(
	v3Dep *kumoriv1.V3Deployment, deploymentField string,
) (
	kukuLinks []*kumoriv1.KukuLink, err error,
) {
	err = nil
	name := v3Dep.GetName()
	ns := v3Dep.GetNamespace()
	matchLabels := map[string]string{deploymentField: name}
	labelSelector := labels.SelectorFromSet(matchLabels)
	list, err := k.kukuLinkLister.KukuLinks(ns).List(labelSelector)
	if err != nil {
		return
	}

	kukuLinks = list
	return
}

type ServiceMeta []Meta
type Meta struct {
	Auto map[string]interface{} `json:"auto"`
	User User                   `json:"user"`
}

type User struct {
	Ingress Ingress `json:"__ingress"`
}

type Ingress struct {
	Subpath      Subpath `json:"subpath"`
	CertRequired bool    `json:"certrequired,omitempty"`
}

type Subpath struct {
	Prefix      *string `json:"prefix"`
	PrefixRegex *bool   `json:"prefix_regex,omitempty"`
	Rewrite     Rewrite `json:"rewrite"`
}

type Rewrite struct {
	Pattern      string `json:"pattern,omitempty"`
	RewriteRegex bool   `json:"rewrite_regex,omitempty"`
	Substitution string `json:"substitution"`
}

type ConnectorInfo struct {
	Name string
	Meta ServiceMeta
}

type KubeServiceInfo struct {
	Name string
	Port int
	Meta ServiceMeta
}

func (serviceMeta *ServiceMeta) SetIngressDefaults() {
	for i, meta := range *serviceMeta {
		if meta.User.Ingress.Subpath.Prefix == nil {
			defaultPrefix := "/.*"
			(*serviceMeta)[i].User.Ingress.Subpath.Prefix = &defaultPrefix
		}

		if meta.User.Ingress.Subpath.PrefixRegex == nil {
			defaultPrefixRegex := true
			(*serviceMeta)[i].User.Ingress.Subpath.PrefixRegex = &defaultPrefixRegex
		}
	}
}
