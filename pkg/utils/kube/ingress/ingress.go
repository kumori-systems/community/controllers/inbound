/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package ingress

import (
	"context"
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	naming "httpinbound-controller/pkg/utils/naming"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/networking/v1"
	record "k8s.io/client-go/tools/record"
)

// IngressTools includes functions related to kube ingresses
type IngressTools struct {
	clientset clientset.Interface
	lister    listers.IngressLister
	recorder  record.EventRecorder
}

// NewIngressTools returns a new IngressTools
func NewIngressTools(
	clientset clientset.Interface,
	lister listers.IngressLister,
	recorder record.EventRecorder,
) (t IngressTools) {
	t = IngressTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of ingreses.
// If action is 'delete', a deleteOptions can be provided
func (t IngressTools) ExecuteActionInMap(
	action string,
	ingresses map[string]*networkingv1.Ingress,
	primaryObject runtime.Object,
	pendingRetryErrors *bool,
) (kerr error) {

	meth := "IngressTools.ExecuteActionInMap()"

	if ingresses == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Mappings is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range ingresses {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, primaryObject)
		case common.ActionUpdate:
			err = t.Update(v, primaryObject)
		case common.ActionDelete:
			err = t.Delete(v, primaryObject)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		shouldAbort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if shouldAbort {
			return errToReturn
		}
	}

	return
}

// Create adds the kube-ingress
func (t IngressTools) Create(
	ingress *networkingv1.Ingress,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "IngressTools.Create()"

	if ingress == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Ingress is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := ingress.GetName()
	ns := ingress.GetNamespace()
	log.Infof("%s ingress %s to be added", meth, name)

	found, err := t.Exist(ingress)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s Ingress %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kuinbound",
	}
	ingress, err = t.clientset.NetworkingV1().Ingresses(ns).Create(ctx, ingress, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "Ingres", ingress.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s Ingress %s created", meth, name)
	return
}

// Update updates the kube-ingress
func (t IngressTools) Update(
	ingress *networkingv1.Ingress,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "IngressTools.Update()"

	if ingress == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Ingress is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := ingress.GetName()
	ns := ingress.GetNamespace()
	log.Infof("%s ingress %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kuinbound",
	}
	ingress, err := t.clientset.NetworkingV1().Ingresses(ns).Update(ctx, ingress, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "Ingres", ingress.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s Ingress %s updated", meth, name)

	return
}

// Delete removes the kube-ingress
func (t IngressTools) Delete(
	ingress *networkingv1.Ingress,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "IngressTools.Delete()"

	if ingress == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Ingress is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := ingress.GetName()
	ns := ingress.GetNamespace()
	log.Infof("%s ingress %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.NetworkingV1().Ingresses(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "Ingres", ingress.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s Ingress %s removed", meth, name)

	return
}

// Exists checks if the mapping already exists
func (t IngressTools) Exist(ingress *networkingv1.Ingress) (bool, error) {
	meth := "IngressTools.Exist()"
	if ingress == nil {
		return false, nil
	}
	name := ingress.GetName()
	ns := ingress.GetNamespace()
	foundIngress, err := t.lister.Ingresses(ns).Get(name)
	if errors.IsNotFound(err) || foundIngress == nil {
		return false, nil
	}
	if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if foundIngress != nil {
		return true, nil
	}
	return false, nil
}

// GetCurrentMap returns the map of ingresses currently related to an owner
// object (searchs using the owner label and checks the ownerref) indexed by
// the internal owner name
func (t IngressTools) GetCurrentMap(owner metav1.Object) (
	currentIngresses map[string]*networkingv1.Ingress, kerr error) {

	meth := "IngressTools.GetCurrentMap()"
	currentIngresses = make(map[string]*networkingv1.Ingress)

	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.Ingresses(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentIngresses[v.GetName()] = v
		} else {
			// This kube-ingress hasn't the provided object as OwnerRef!
			msg := "Ingress " + v.GetName() + " is not controlled by " + owner.GetName()
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			log.Errorf("%s %s", meth, kerr.Error())
			return
		}
	}

	return
}

// MergeToCurrent copy properties of newobj into currentobj
func (t IngressTools) MergeToCurrent(
	currentobj *networkingv1.Ingress, newobj *networkingv1.Ingress,
) *networkingv1.Ingress {
	currentobj.Spec = *newobj.Spec.DeepCopy()
	currentobj.Annotations[common.ExternalDnsAnnotation] = newobj.Annotations[common.ExternalDnsAnnotation]
	return currentobj
}

// Equal returns true if two mappings are equivalent. Is not enough to compare
// spec field: annotations.external-dns.alpha.kubernetes.io/target must be
// compared too.
func (t IngressTools) Equal(
	o1, o2 *networkingv1.Ingress,
) bool {
	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}
	if o1.Annotations[common.ExternalDnsAnnotation] != o2.Annotations[common.ExternalDnsAnnotation] {
		return false
	}
	return apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)
}

// GenerateIngress creates a new Ingress related to the inbound .
// Example of generated object:
//
//	apiVersion: networking/v1
//	kind: Ingress
//	metadata:
//	  annotations:
//	    external-dns.alpha.kubernetes.io/target: 172.31.255.100,172.31.255.101
//	  labels:
//	    owner: httpinbound-789
//	  name: httpinbound-789-extdns-ingress
//	  namespace: myns
//	  ownerReferences: [...]
//	spec:
//	  rules:
//	  - host: one.kumori.cloud
//	  - host: two.other.es
//	  - host: three.kumori.cloud
//	status:
//	  loadBalancer: {}
func (t IngressTools) GenerateIngress(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	controllerConfig common.ControllerConfig,
) (
	ingress *networkingv1.Ingress,
) {

	meth := "IngressTools.GenerateIngress()"
	ownerNamespace := v3Dep.GetNamespace()
	ownerName := v3Dep.GetName()
	ingressName := naming.CalculateIngressName(ownerName)
	log.Infof("%s %s", meth, ingressName)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = ownerName

	annotations := make(map[string]string)
	nodes := t.calculateNodes(v3Dep, httpInboundConfig, controllerConfig)
	annotations[common.ExternalDnsAnnotation] = nodes

	meta := metav1.ObjectMeta{
		Name:      ingressName,
		Namespace: ownerNamespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(v3Dep, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindV3Deployment,
			}),
		},
	}
	meta.SetLabels(labels)
	meta.SetAnnotations(annotations)

	rules := t.calculateRules(httpInboundConfig)
	spec := networkingv1.IngressSpec{
		Rules: rules,
	}

	ingress = &networkingv1.Ingress{
		ObjectMeta: meta,
		Spec:       spec,
	}

	return
}

// calculateNodes returns the list of nodes related to the kukuhttpinbound
// object as a string (format "a,b,c")
//
// TEMPORARY PATCH: ticket161
func (t IngressTools) calculateNodes(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	controllerConfig common.ControllerConfig,
) (
	nodes string,
) {
	nodes = controllerConfig.IngressCNAME
	return
}

// calculateRules returns the list of rules related to the kukuhttpinbound
// object a slice of networkingv1.IngressRule's
func (t IngressTools) calculateRules(
	httpInboundConfig *inboundconfig.HttpInboundConfig,
) (
	rules []networkingv1.IngressRule,
) {
	rules = make([]networkingv1.IngressRule, 1) // Currently, only one domain per inbound is allowed
	rules[0] = networkingv1.IngressRule{Host: httpInboundConfig.Domain.Spec.Domain}
	return
}
