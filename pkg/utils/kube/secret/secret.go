/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package secret

import (
	"context"
	"fmt"

	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	naming "httpinbound-controller/pkg/utils/naming"

	log "github.com/sirupsen/logrus"
	base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"
	corev1 "k8s.io/api/core/v1"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	clientset "k8s.io/client-go/kubernetes"
	listers "k8s.io/client-go/listers/core/v1"
	record "k8s.io/client-go/tools/record"
)

// SecretTools includes functions related to kube secrets
type SecretTools struct {
	clientset clientset.Interface
	lister    listers.SecretLister
	recorder  record.EventRecorder
}

// NewSecretTools returns a new SecretTools
func NewSecretTools(
	clientset clientset.Interface,
	lister listers.SecretLister,
	recorder record.EventRecorder,
) (t SecretTools) {
	t = SecretTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of tlscontext.
// If action is 'delete', a deleteOptions can be provided.
// The action is related to a primary object.
func (t SecretTools) ExecuteActionInMap(
	action string,
	secrets map[string]*corev1.Secret,
	primaryObject runtime.Object,
	pendingRetryErrors *bool,
) (kerr error) {
	meth := "SecretTools.ExecuteActionInMap()"

	if secrets == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Secrets is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range secrets {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, primaryObject)
		case common.ActionUpdate:
			err = t.Update(v, primaryObject)
		case common.ActionDelete:
			err = t.Delete(v, primaryObject)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if abort {
			return errToReturn
		}
	}

	return
}

// Create adds the kubesecret
func (t SecretTools) Create(
	secret *corev1.Secret,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "SecretTools.Create()"

	if secret == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Secret is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := secret.GetName()
	ns := secret.GetNamespace()
	log.Infof("%s secret %s to be added", meth, name)

	found, err := t.Exist(secret)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s Secret %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kuinbound",
	}
	secret, err = t.clientset.CoreV1().Secrets(ns).Create(ctx, secret, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "Secret", secret.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s Secret %s created", meth, name)
	return
}

// Update updates the kubesecret
func (t SecretTools) Update(
	secret *corev1.Secret,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "SecretTools.Update()"

	if secret == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Secret is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := secret.GetName()
	ns := secret.GetNamespace()
	log.Infof("%s secret %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	secret, err := t.clientset.CoreV1().Secrets(ns).Update(ctx, secret, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "Secret", secret.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s Secret %s updated", meth, name)

	return
}

// Delete removes the kubesecret
func (t SecretTools) Delete(
	secret *corev1.Secret,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "SecretTools.Delete()"

	if secret == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Secret is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := secret.GetName()
	ns := secret.GetNamespace()
	log.Infof("%s secret %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.CoreV1().Secrets(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "Secret", secret.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s Secret %s removed", meth, name)

	return
}

// Exists checks if the secret already exists
func (t SecretTools) Exist(secret *corev1.Secret) (bool, error) {
	meth := "SecretTools.Exist()"
	if secret == nil {
		return false, nil
	}
	name := secret.GetName()
	ns := secret.GetNamespace()
	foundSecret, err := t.lister.Secrets(ns).Get(name)
	if errors.IsNotFound(err) || foundSecret == nil {
		return false, nil
	}
	if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if foundSecret != nil {
		return true, nil
	}
	return false, nil
}

// GetCurrentList returns the list of secrets currently related to
// an owner object (tipically, a kukucert object): searchs using the owner label
// and checks ownerRef value
func (t SecretTools) GetCurrentList(owner metav1.Object) (
	currentSecrets []*corev1.Secret, kerr error) {

	meth := "SecretTools.GetCurrentList()"
	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.Secrets(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentSecrets = append(currentSecrets, v)
		} else {
			// This kube-secret hasn't the provided object as OwnerRef!
			msg := fmt.Sprintf(base.MessageErrResourceExists, name)
			log.Warnf("%s %s", meth, msg)
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
	}

	return
}

// GetCurrentMap returns the map of kubesecrets (indexed by name) currently
// related to an ownwer object (tipically, a kukucert object): searchs using
// the owner label and checks ownerRef value
func (t SecretTools) GetCurrentMap(owner metav1.Object) (
	currentSecrets map[string]*corev1.Secret, kerr error) {

	meth := "SecretTools.GetCurrentMap()"
	currentSecrets = make(map[string]*corev1.Secret)
	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.Secrets(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentSecrets[v.GetName()] = v
		} else {
			// This kube-secret hasn't the provided object as OwnerRef!
			msg := fmt.Sprintf(base.MessageErrResourceExists, name)
			log.Warnf("%s %s", meth, msg)
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
	}

	return
}

// GenerateSecretFromCA creates a new secret related to the kukuca resource
// containing a server CA secret
func (t SecretTools) GenerateSecretFromCA(kukuca *kumoriv1.KukuCa) *corev1.Secret {
	meth := "SecretTools.GenerateSecretFromCA()"
	ns := kukuca.GetNamespace()
	name := kukuca.GetName()
	secretName := naming.CalculateCASecretName(name)
	log.Infof("%s %s", meth, secretName)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = name

	// "tls.crt": where Ambassador/Envoy expects it
	// https://www.getambassador.io/docs/latest/howtos/client-cert-validation/
	data := map[string][]byte{
		"tls.crt": kukuca.Spec.Ca,
	}
	meta := metav1.ObjectMeta{
		Name:      secretName,
		Namespace: ns,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukuca, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuCA,
			}),
		},
	}
	meta.SetLabels(labels)

	secret := &corev1.Secret{
		ObjectMeta: meta,
		Data:       data,
		Type:       "Opaque",
	}
	return secret
}

// GenerateSecretFromCert creates a new secret related to the kukucert resource
// containing a server cert secret
func (t SecretTools) GenerateSecretFromCert(kukucert *kumoriv1.KukuCert) *corev1.Secret {
	meth := "SecretTools.GenerateSecretFromCert()"
	ns := kukucert.GetNamespace()
	name := kukucert.GetName()
	secretName := naming.CalculateCertSecretName(name)
	log.Infof("%s %s", meth, secretName)

	annotations := make(map[string]string)
	annotations[common.CommentAnnotation] = fmt.Sprintf("Related to host %s", kukucert.Data.Domain)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = name

	data := map[string][]byte{
		"tls.crt": kukucert.Data.Cert,
		"tls.key": kukucert.Data.Key,
	}
	meta := metav1.ObjectMeta{
		Name:      secretName,
		Namespace: ns,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukucert, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuCert,
			}),
		},
	}
	meta.SetLabels(labels)
	meta.SetAnnotations(annotations)

	secret := &corev1.Secret{
		ObjectMeta: meta,
		Data:       data,
		Type:       "kubernetes.io/tls",
	}
	return secret
}
