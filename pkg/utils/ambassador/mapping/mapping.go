/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package mapping

import (
	"context"
	"encoding/json"
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	helper "httpinbound-controller/pkg/utils/helper"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	"httpinbound-controller/pkg/utils/kuku/v3deployment"
	naming "httpinbound-controller/pkg/utils/naming"
	"strconv"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	// base "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/base-controller"

	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	amblisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/listers/getambassador.io/v3alpha1"

	corev1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	record "k8s.io/client-go/tools/record"
)

// MappingTools includes functions related to Ambassador mappings
type MappingTools struct {
	clientset ambclientset.Interface
	lister    amblisters.MappingLister
	recorder  record.EventRecorder
}

// NewMappingTools returns a new MappingTools
func NewMappingTools(
	clientset ambclientset.Interface,
	lister amblisters.MappingLister,
	recorder record.EventRecorder,
) (t MappingTools) {
	t = MappingTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of mappings.
// If action is 'delete', a deleteOptions can be provided
func (t MappingTools) ExecuteActionInMap(
	action string,
	mappings map[string]*ambv3alpha1.Mapping,
	primaryObject runtime.Object,
	pendingRetryErrors *bool,
) (kerr error) {

	meth := "MappingTools.ExecuteActionInMap()"

	if mappings == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Mappings is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range mappings {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, primaryObject)
		case common.ActionUpdate:
			err = t.Update(v, primaryObject)
		case common.ActionDelete:
			err = t.Delete(v, primaryObject)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if abort {
			return errToReturn
		}
	}

	return
}

// Create adds the mapping
func (t MappingTools) Create(
	mapping *ambv3alpha1.Mapping,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "MappingTools.Create()"

	if mapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Mapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := mapping.GetName()
	ns := mapping.GetNamespace()
	log.Infof("%s mapping %s to be added", meth, name)

	found, err := t.Exist(mapping)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s Mapping %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: common.FieldManager,
	}
	mapping, err = t.clientset.GetambassadorV3alpha1().Mappings(ns).Create(ctx, mapping, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "Mapping", mapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s Mapping %s created", meth, name)
	return
}

// Update updates the mapping
func (t MappingTools) Update(
	mapping *ambv3alpha1.Mapping,
	primaryObject runtime.Object,
) (kerr error) {
	meth := "MappingTools.Update()"

	if mapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Mapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := mapping.GetName()
	ns := mapping.GetNamespace()
	log.Infof("%s mapping %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	mapping, err := t.clientset.GetambassadorV3alpha1().Mappings(ns).Update(ctx, mapping, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "Mapping", mapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s Mapping %s updated", meth, name)

	return
}

// Delete removes the mapping
func (t MappingTools) Delete(
	mapping *ambv3alpha1.Mapping,
	primaryObject runtime.Object,
) (kerr error) {
	meth := "MappingTools.Delete()"

	if mapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Mapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := mapping.GetName()
	ns := mapping.GetNamespace()
	log.Infof("%s mapping %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.GetambassadorV3alpha1().Mappings(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "Mapping", mapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s Mapping %s removed", meth, name)

	return
}

// Exists checks if the mapping already exists
func (t MappingTools) Exist(mapping *ambv3alpha1.Mapping) (bool, error) {
	meth := "MappingTools.Exist()"
	if mapping == nil {
		return false, nil
	}
	name := mapping.GetName()
	ns := mapping.GetNamespace()
	foundMapping, err := t.lister.Mappings(ns).Get(name)
	if errors.IsNotFound(err) || foundMapping == nil {
		return false, nil
	}
	if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if foundMapping != nil {
		return true, nil
	}
	return false, nil
}

// GetCurrentMap returns the map of mappings currently related to an owner
// object (searchs using the owner label and checks the ownerref) indexed by
// the internal owner name
func (t MappingTools) GetCurrentMap(
	owner metav1.Object,
) (
	currentMappings map[string]*ambv3alpha1.Mapping, kerr error,
) {

	meth := "MappingTools.GetCurrentMap()"
	currentMappings = make(map[string]*ambv3alpha1.Mapping)

	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.Mappings(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentMappings[v.GetName()] = v
		} else {
			// This mapping hasn't the provided object as OwnerRef!
			msg := "Mapping " + v.GetName() + " is not controlled by " + owner.GetName()
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			log.Errorf("%s %s", meth, kerr.Error())
			return
		}
	}

	return
}

// MergeToCurrent copy properties of newobj into currentobj
func (t MappingTools) MergeToCurrent(
	currentobj *ambv3alpha1.Mapping, newobj *ambv3alpha1.Mapping,
) *ambv3alpha1.Mapping {
	currentobj.Spec = *newobj.Spec.DeepCopy()
	return currentobj
}

// Equal returns true if two mappings are equivalent. Just spec is compared.
func (t MappingTools) Equal(
	o1, o2 *ambv3alpha1.Mapping,
) bool {
	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}
	return apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)
}

// GenerateUnlinkedMapping creates a new mapping related to the an httpinbound.
// Example of generated object:
//
//		apiVersion: getambassador.io/v3alpha1
//		kind: Mapping
//		metadata:
//		  name: kd-074321-9a434ebe-mapping-unlinked
//		  namespace: kumori
//		  labels:
//		     kumori/owner: kd-074321-9a434ebe
//		  ownerReferences: [...]
//		spec:
//	    ambassadorID
//	    - --apiVersion-v3alpha1-only--default  (setted just as "default" in code)
//		  hostname: hello.test.kumori.cloud
//		  load_balancer:
//		    policy: round_robin
//		  precedence: 1
//		  prefix: /.*.*
//		  prefix_regex: true
//		  rewrite: ""
//		  resolver: kumori-default
//		  service: outofservice.kumoriingress:5000
//	    timeout_ms: 36000000
//	    idle_timeout_ms: 36000000
//	    add_request_headers: map[string]{}  (empty for unlinked)
//	    labels: ambv3alpha1.DomainMap{} (depends on ip filtering)
func (t *MappingTools) GenerateUnlinkedMapping(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
) (
	mapping *ambv3alpha1.Mapping,
) {
	meth := "MappingTools.GenerateUnlinkedMapping()"
	ownerNamespace := v3Dep.GetNamespace()
	ownerName := v3Dep.GetName()
	mappingName := naming.CalculateUnlinkMappingName(ownerName)
	log.Infof("%s %s", meth, mappingName)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = ownerName

	meta := metav1.ObjectMeta{
		Name:      mappingName,
		Namespace: ownerNamespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(v3Dep, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindV3Deployment,
			}),
		},
	}
	meta.SetLabels(labels)

	// Generates the Mapping spec
	//
	// About a "go vet" error emitted by this code:
	// There is a problem with the type "ambv3alpha1.MillisecondDuration" used
	// in the MappingSpec, defined in getambassador.io/v3alpha1/common.go as:
	//   type MillisecondDuration struct {
	//   	time.Duration `json:"-"`
	//   }
	// When "go vet" is executed, an error "MillisecondDuration composite literal
	// uses unkeyed fields" is emitted.
	// The error is only emitted when the MillisecondDuration is defined in a
	// package. If MillisecondDuration is defined in this file (mapping.go), the
	// error is not emitted!
	// Related issues:
	// https://github.com/golang/go/issues/9171
	// https://github.com/prometheus/prometheus/pull/1029
	// https://github.com/golang/go/issues/15408
	// Can I "mark" specific lines to disallow specific vet errors? no:
	// https://github.com/golang/go/issues/10752
	//
	domain := httpInboundConfig.Domain.Spec.Domain
	// Experiment task1734
	// loadbalancer := &ambv3alpha1.LoadBalancer{Policy: "round_robin"}
	addrequestheaders := make(map[string]ambv3alpha1.AddedHeader) // empty, while unlinked
	trueValue := true
	spec := ambv3alpha1.MappingSpec{
		AmbassadorID: common.AmbassadorID(),
		Hostname:     domain,
		// Experiment task1734
		// LoadBalancer:      loadbalancer,
		Precedence:  helper.IntPtr(1), // Outofservice has the low precedence
		Prefix:      "/.*",            // Mandatory to use "precedence" funcionality in Ambassador
		PrefixRegex: helper.BoolPtr(true),
		Rewrite:     helper.StrPtr(""),
		// Experiment task1734
		// Resolver:          common.KeprName,
		Resolver:          common.KeprServiceName,
		BypassAuth:        &trueValue,
		Service:           common.OutOfServiceApp + "." + ownerNamespace + ":" + common.OutOfServicePortStr,
		Timeout:           &ambv3alpha1.MillisecondDuration{common.RequestTimeout()},
		IdleTimeout:       &ambv3alpha1.MillisecondDuration{common.IdleTimeout()},
		AddRequestHeaders: &addrequestheaders,
		Labels:            t.createDomainMapFromInbound(httpInboundConfig),
	}

	mapping = &ambv3alpha1.Mapping{
		ObjectMeta: meta,
		Spec:       spec,
	}

	return
}

// GenerateLinkedMapping creates a new mapping related to a kukulink.
// Example of generated object:
//
//	apiVersion: getambassador.io/v3alpha1
//	kind: Mapping
//	metadata:
//	  name: kl-8e42cbe9e00014eed16021eaa0432ae192af6989-mapping-linked
//	  namespace: kumori
//	  labels:
//	    owner: kl-8e42cbe9e00014eed16021eaa0432ae192af6989
//	  ownerReferences: [...]
//	spec:
//	   ambassadorID
//	   - --apiVersion-v3alpha1-only--default  (setted just as "default" in code)
//	  hostname: hello.test.kumori.cloud
//	  load_balancer:
//	    policy: round_robin
//	  precedence: 2
//	  prefix: /.*
//	  prefix_regex: true
//	  rewrite: ""
//	  resolver: kumori-default
//	  service: kd-171632-61486294-3c8b3424-lb-service.kumori:80
//	   cluster_tag: dbe794b7
//	  timeout_ms: 3600000
//	   idle_timeout_ms: 36000000
//	  allowUpgrade: [] / [websocket]
//	  add_request_headers:
//	    x-forwarded-client-cert: "%DOWNSTREAM_PEER_CERT%"
//	   labels: ambv3alpha1.DomainMap{} (depends on ip filtering)
func (t *MappingTools) GenerateLinkedMapping(
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	kukulink *kumoriv1.KukuLink,
	kubeServiceName string,
	kubeServicePort int,
	connectorMeta v3deployment.ServiceMeta,
) (
	mappingName string, mapping *ambv3alpha1.Mapping,
) {
	meth := "MappingTools.GenerateLinkedMapping()"
	ownerNamespace := kukulink.GetNamespace()
	ownerName := kukulink.GetName()
	mappingName = naming.CalculateLinkMappingName(ownerName, kubeServiceName)
	log.Infof("%s %s", meth, mappingName)
	labels := make(map[string]string)
	labels[common.OwnerLabel] = ownerName

	meta := metav1.ObjectMeta{
		Name:      mappingName,
		Namespace: ownerNamespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukulink, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuLink,
			}),
		},
	}
	meta.SetLabels(labels)

	// Generates the Mapping spec
	//
	// About clusterTag: this is the crc of the a string that contains the ID of
	// the v3dep and the kube-service.
	// Why use "ClusterTag"?
	// - https://gitlab.com/kumori/kv3/project-management/-/issues/862
	// - https://github.com/emissary-ingress/emissary/issues/3112
	//   (it is not clear if using Ambassador v3 is still an error, but the
	//   clustertag is fine in any case)
	//
	// About a "go vet" error emitted by this code, related to the
	// MillisecondDuration object: see comments in the "GenerateUnlinkedMapping"
	// function.
	//
	domain := httpInboundConfig.Domain.Spec.Domain
	// Experiment task1734
	// loadbalancer := &ambv3alpha1.LoadBalancer{Policy: "round_robin"}
	addrequestheaders := make(map[string]ambv3alpha1.AddedHeader)
	if httpInboundConfig.ClientCert {
		// TBD: the name of header should be configurable in kuinbound manifest
		addrequestheaders["x-forwarded-client-cert"] = ambv3alpha1.AddedHeader{
			Value: "%DOWNSTREAM_PEER_CERT%",
		}
	}
	if httpInboundConfig.RemoteAddressHeader != "" {
		addrequestheaders[httpInboundConfig.RemoteAddressHeader] = ambv3alpha1.AddedHeader{
			Value: "%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%",
		}
	}
	if httpInboundConfig.CleanXForwardedFor {
		falseValue := false
		addrequestheaders["x-forwarded-for"] = ambv3alpha1.AddedHeader{
			Append: &falseValue,
			Value:  "%DOWNSTREAM_REMOTE_ADDRESS_WITHOUT_PORT%",
		}
	}

	mappingService := kubeServiceName + "." + ownerNamespace + ":" + strconv.Itoa(kubeServicePort)
	longClusterTag := httpInboundConfig.ID + "/" + mappingService
	clusterTag := helper.CrcString(longClusterTag)
	allowUpgrade := []string{}
	if httpInboundConfig.Websocket {
		allowUpgrade = []string{"websocket"}
	}

	spec := ambv3alpha1.MappingSpec{
		AmbassadorID: common.AmbassadorID(),
		Hostname:     domain,
		// Experiment task1734
		//LoadBalancer:      loadbalancer,
		Precedence:  helper.IntPtr(2),                              // Outofservice has a low precedence
		Prefix:      *connectorMeta[0].User.Ingress.Subpath.Prefix, // Mandatory to use "precedence" functionality in Ambassador
		PrefixRegex: connectorMeta[0].User.Ingress.Subpath.PrefixRegex,
		// Experiment task1734
		//Resolver:          common.KeprName,
		Resolver:          common.KeprServiceName,
		Service:           mappingService,
		ClusterTag:        clusterTag,
		Timeout:           &ambv3alpha1.MillisecondDuration{common.RequestTimeout()},
		IdleTimeout:       &ambv3alpha1.MillisecondDuration{common.IdleTimeout()},
		AllowUpgrade:      allowUpgrade,
		AddRequestHeaders: &addrequestheaders,
		Labels:            t.createDomainMapFromInbound(httpInboundConfig),
	}

	if connectorMeta[0].User.Ingress.Subpath.Rewrite.RewriteRegex {
		spec.RegexRewrite = &ambv3alpha1.RegexMap{
			Pattern:      connectorMeta[0].User.Ingress.Subpath.Rewrite.Pattern,
			Substitution: connectorMeta[0].User.Ingress.Subpath.Rewrite.Substitution,
		}
	} else {
		spec.Rewrite = &connectorMeta[0].User.Ingress.Subpath.Rewrite.Substitution
	}

	var bypassAuthValue bool
	if httpInboundConfig.ClientCert && httpInboundConfig.CertRequired {
		// AuthService can be bypassed as Host.tls.CertRequired will be true.
		bypassAuthValue = true
		spec.BypassAuth = &bypassAuthValue
	} else if httpInboundConfig.ClientCert {
		bypassAuthValue = !connectorMeta[0].User.Ingress.CertRequired
		spec.BypassAuth = &bypassAuthValue
	} else {
		bypassAuthValue = true
		spec.BypassAuth = &bypassAuthValue
	}

	mapping = &ambv3alpha1.Mapping{
		ObjectMeta: meta,
		Spec:       spec,
	}

	return
}

// createDomainMapFromInbound creates the DomainMap object used in mappings,
// containing the access policies defined in the inbound.
// DomainMap definition: see "type DomainMap map[string]MappingLabelGroupsArray"
// in the crd_mapping.go file (Ambassador client-go)
//
// In access policies are present, the value to be returned is (yaml sintax):
//
//		 # Domain name: useful when there are several ambassador using the ratelimit
//		 # service; we use just the tag "ambassador"
//			ambassador:
//		   # Label group: we use just one group, and we named it just "request_label_group"
//			  - request_label_group:
//	       # We want that Ambassador sends the remote_address to the ratelimit service
//			    - remote_address
//	       # Just the inbound config access policies stringified
//			    - generic_key: access_policies: '{"allowediplist":["192.168.20.152"]}'
//
// Based on:
// https://www.getambassador.io/docs/emissary/3.6/topics/running/services/rate-limit-service
func (t *MappingTools) createDomainMapFromInbound(
	httpInboundConfig *inboundconfig.HttpInboundConfig,
) (
	domainMap ambv3alpha1.DomainMap,
) {
	meth := "MappingTools.createDomainMapFromInbound()"

	accessPoliciesStr := ""
	if httpInboundConfig.AccessPolicies != nil {
		accessPoliciesBytes, err := json.Marshal(httpInboundConfig.AccessPolicies)
		if err != nil {
			log.Errorf("%s %s", meth, err.Error())
			return
		}
		accessPoliciesStr = string(accessPoliciesBytes)
	}

	if accessPoliciesStr != "" {

		// Label to add the remote address
		labelSpecifier1 := ambv3alpha1.MappingLabelSpecifier{
			RemoteAddress: &ambv3alpha1.MappingLabelSpecifier_RemoteAddress{
				Key: "remote_address",
			},
		}

		// Label to add the access policies
		labelSpecifier2 := ambv3alpha1.MappingLabelSpecifier{
			GenericKey: &ambv3alpha1.MappingLabelSpecifier_GenericKey{
				Value: "access_policies: " + accessPoliciesStr,
			},
		}

		// Label "host" to add host header (the request domain)
		// This is not required for rate-limiting, but it is useful because the
		// domain is logged in the rate-limit service
		labelSpecifier3 := ambv3alpha1.MappingLabelSpecifier{
			RequestHeaders: &ambv3alpha1.MappingLabelSpecifier_RequestHeaders{
				Key:        "host",
				HeaderName: "host",
			},
		}

		// Create the array containint all labels
		labelsArray := ambv3alpha1.MappingLabelsArray{
			labelSpecifier1,
			labelSpecifier2,
			labelSpecifier3,
		}

		labelGroup := ambv3alpha1.MappingLabelGroup{
			"request_label_group": labelsArray,
		}

		labelGroupArray := ambv3alpha1.MappingLabelGroupsArray{
			labelGroup,
		}

		domainMap = ambv3alpha1.DomainMap{
			"ambassador": labelGroupArray,
		}
	}

	return
}
