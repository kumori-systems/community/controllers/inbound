/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package host

import (
	"context"
	"fmt"

	// "encoding/json"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	naming "httpinbound-controller/pkg/utils/naming"

	// "strconv"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	amblisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/listers/getambassador.io/v3alpha1"

	corev1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	"k8s.io/apimachinery/pkg/runtime/schema"

	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
	record "k8s.io/client-go/tools/record"
)

// HostTools includes functions related to Ambassador hosts
type HostTools struct {
	clientset ambclientset.Interface
	lister    amblisters.HostLister
	recorder  record.EventRecorder
}

// NewHostTools returns a new HostTools
func NewHostTools(
	clientset ambclientset.Interface,
	lister amblisters.HostLister,
	recorder record.EventRecorder,
) (t HostTools) {
	t = HostTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of hosts.
// If action is 'delete', a deleteOptions can be provided
func (t HostTools) ExecuteActionInMap(
	action string,
	hosts map[string]*ambv3alpha1.Host,
	primaryObject runtime.Object,
	pendingRetryErrors *bool,
) (kerr error) {

	meth := "HostTools.ExecuteActionInMap()"

	if hosts == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Hosts is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range hosts {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, primaryObject)
		case common.ActionUpdate:
			err = t.Update(v, primaryObject)
		case common.ActionDelete:
			err = t.Delete(v, primaryObject)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if abort {
			return errToReturn
		}
	}

	return
}

// Create adds the host
func (t HostTools) Create(
	host *ambv3alpha1.Host,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "HostTools.Create()"

	if host == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Host is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := host.GetName()
	ns := host.GetNamespace()
	log.Infof("%s host %s to be added", meth, name)

	found, err := t.Exist(host)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s Host %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: common.FieldManager,
	}
	host, err = t.clientset.GetambassadorV3alpha1().Hosts(ns).Create(ctx, host, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "Host", host.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s Host %s created", meth, name)
	return
}

// Update updates the host
func (t HostTools) Update(
	host *ambv3alpha1.Host,
	primaryObject runtime.Object,
) (kerr error) {
	meth := "HostTools.Update()"

	if host == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Host is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := host.GetName()
	ns := host.GetNamespace()
	log.Infof("%s host %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: common.FieldManager,
	}
	host, err := t.clientset.GetambassadorV3alpha1().Hosts(ns).Update(ctx, host, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "Host", host.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s Host %s updated", meth, name)

	return
}

// Delete removes the host
func (t HostTools) Delete(
	host *ambv3alpha1.Host,
	primaryObject runtime.Object,
) (kerr error) {
	meth := "HostTools.Delete()"

	if host == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "Host is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := host.GetName()
	ns := host.GetNamespace()
	log.Infof("%s host %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.GetambassadorV3alpha1().Hosts(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "Host", host.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s Host %s removed", meth, name)

	return
}

// Exists checks if the host already exists
func (t HostTools) Exist(host *ambv3alpha1.Host) (bool, error) {
	meth := "HostTools.Exist()"
	if host == nil {
		return false, nil
	}
	name := host.GetName()
	ns := host.GetNamespace()
	foundHost, err := t.lister.Hosts(ns).Get(name)
	if errors.IsNotFound(err) || foundHost == nil {
		return false, nil
	}
	if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if foundHost != nil {
		return true, nil
	}
	return false, nil
}

// GetCurrentMap returns the map of hosts currently related to an owner
// object (searchs using the owner label and checks the ownerref) indexed by
// the internal owner name
func (t HostTools) GetCurrentMap(
	owner metav1.Object,
) (
	currentHosts map[string]*ambv3alpha1.Host, kerr error,
) {

	meth := "HostTools.GetCurrentMap()"
	currentHosts = make(map[string]*ambv3alpha1.Host)

	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.Hosts(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentHosts[v.GetName()] = v
		} else {
			// This host hasn't the provided object as OwnerRef!
			msg := "Host " + v.GetName() + " is not controlled by " + owner.GetName()
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			log.Errorf("%s %s", meth, kerr.Error())
			return
		}
	}

	return
}

// MergeToCurrent copy properties of newobj into currentobj
func (t HostTools) MergeToCurrent(
	currentobj *ambv3alpha1.Host, newobj *ambv3alpha1.Host,
) *ambv3alpha1.Host {
	currentobj.Spec = newobj.Spec.DeepCopy()
	return currentobj
}

// Equal returns true if two hosts are equivalent. Just spec is compared.
func (t HostTools) Equal(
	o1, o2 *ambv3alpha1.Host,
) bool {
	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}
	return apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)
}

// GenerateHost creates a new host related to the an httpinbound.
// Example of generated object:
//
//		apiVersion: getambassador.io/v3alpha1
//		kind: Host
//		metadata:
//		  name: kd-123456-host
//		  namespace: kumori
//		  labels:
//		    owner: httpinbound-789
//	      listener-type: HTTPS
//		  ownerReferences: [...]
//		spec:
//		  hostname: juanjo.test.kumori.cloud
//		  tlsSecret:
//		    name: cluster-core-cert-wildcard-test-kumori-cloud-cert
//	    tls:
//	      ca_secret: ""
//	      cipher_suites:
//	      - ""
//	      ecdh_curves:
//	      - ""
//	      min_tls_version: v1.2
//	      redirect_cleartext_from: 8080
func (t *HostTools) GenerateHost(
	v3Dep *kumoriv1.V3Deployment,
	httpInboundConfig *inboundconfig.HttpInboundConfig,
	controllerConfig common.ControllerConfig,
) (
	host *ambv3alpha1.Host,
) {
	meth := "HostTools.GenerateHost()"
	ownerNamespace := v3Dep.GetNamespace()
	ownerName := v3Dep.GetName()
	hostName := naming.CalculateHostName(ownerName)
	log.Infof("%s %s", meth, hostName)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = ownerName
	labels[common.ListenerType] = "HTTPS"

	meta := metav1.ObjectMeta{
		Name:      hostName,
		Namespace: ownerNamespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(v3Dep, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindV3Deployment,
			}),
		},
	}
	meta.SetLabels(labels)

	// Get the kubernetes secret related to this host
	secret := corev1.SecretReference{
		Name:      naming.CalculateCertSecretName(httpInboundConfig.Certificate.GetName()),
		Namespace: ownerNamespace,
	}

	// Prepare the tls context info
	tls := ambv3alpha1.TLSConfig{}
	if httpInboundConfig.ClientCert && httpInboundConfig.CA != nil {
		tls.CASecret = naming.CalculateCASecretName(httpInboundConfig.CA.GetName())
		tls.CertRequired = &httpInboundConfig.CertRequired
	}
	if controllerConfig.MinTLSVersion != "" {
		tls.MinTLSVersion = controllerConfig.MinTLSVersion
	}
	if controllerConfig.MaxTLSVersion != "" {
		tls.MaxTLSVersion = controllerConfig.MaxTLSVersion
	}
	if (controllerConfig.CipherSuitesTLS != nil) && (len(controllerConfig.CipherSuitesTLS) > 0) {
		tls.CipherSuites = controllerConfig.CipherSuitesTLS
	}
	if (controllerConfig.EcdhCurvesTLS != nil) && (len(controllerConfig.EcdhCurvesTLS) > 0) {
		tls.ECDHCurves = controllerConfig.EcdhCurvesTLS
	}

	// TBD issue https://gitlab.com/kumori/project-management/-/issues/1083
	// In old Ambassador versions, we used this ...
	//   if controllerConfig.HttpPort != 0 {
	//   	tls.RedirectCleartextFrom = &controllerConfig.HttpPort
	//   }
	// ... but with the new Ambassador v3.x versions, configuration is different:
	// - A 8080-listener must be created to allow http->https redirection
	// - In the host, the redirection is configured via the "requestpolicy"
	// For now, we configure the requestpolicy... but the 8080-listener is not
	// created! To investigate ...  (probably, the "controllerconfig.httpport" has
	// no sense)
	//
	// The default policy is "action=Redirect", but for now we reject insecure
	// request, until issue 1083 will be finished
	//
	requestPolicy := ambv3alpha1.RequestPolicy{
		Insecure: ambv3alpha1.InsecureRequestPolicy{
			Action: "Reject",
		},
	}

	// Generates the Host spec
	// Note: default behaviour is redirect (301) HTTP to HTTPS, but this behaviour
	// can be changed via the "RequestPolicy" property
	spec := ambv3alpha1.HostSpec{
		AmbassadorID:  common.AmbassadorID(),
		Hostname:      httpInboundConfig.Domain.Spec.Domain,
		TLSSecret:     &secret,
		TLS:           &tls,
		RequestPolicy: &requestPolicy,
	}

	host = &ambv3alpha1.Host{
		ObjectMeta: meta,
		Spec:       &spec,
	}

	return
}
