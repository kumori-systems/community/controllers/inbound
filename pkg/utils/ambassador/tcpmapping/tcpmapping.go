/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package tcpmapping

import (
	"context"
	"fmt"
	common "httpinbound-controller/pkg/utils/common"
	kerrors "httpinbound-controller/pkg/utils/errors"
	helper "httpinbound-controller/pkg/utils/helper"
	inboundconfig "httpinbound-controller/pkg/utils/inboundconfig"
	naming "httpinbound-controller/pkg/utils/naming"
	"strconv"

	log "github.com/sirupsen/logrus"
	kumoriv1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis/kumori/v1"

	ambv3alpha1 "gitlab.com/kumori-systems/community/libraries/client-go/pkg/apis_ambassador/getambassador.io/v3alpha1"
	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	amblisters "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/listers/getambassador.io/v3alpha1"

	corev1 "k8s.io/api/core/v1"
	apiequality "k8s.io/apimachinery/pkg/api/equality"
	errors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	labels "k8s.io/apimachinery/pkg/labels"
	runtime "k8s.io/apimachinery/pkg/runtime"
	schema "k8s.io/apimachinery/pkg/runtime/schema"
	record "k8s.io/client-go/tools/record"
)

// TCPMappingTools includes functions related to Ambassador tcptcpmapping
type TCPMappingTools struct {
	clientset ambclientset.Interface
	lister    amblisters.TCPMappingLister
	recorder  record.EventRecorder
}

// NewTCPMappingTools returns a new TCPMappingTools
func NewTCPMappingTools(
	clientset ambclientset.Interface,
	lister amblisters.TCPMappingLister,
	recorder record.EventRecorder,
) (t TCPMappingTools) {
	t = TCPMappingTools{
		clientset: clientset,
		lister:    lister,
		recorder:  recorder,
	}
	return
}

// ExecuteActionInMap executes an action using the provided map of tcpmapping.
// If action is 'delete', a deleteOptions can be provided
func (t TCPMappingTools) ExecuteActionInMap(
	action string,
	tcpmapping map[string]*ambv3alpha1.TCPMapping,
	primaryObject runtime.Object,
	pendingRetryErrors *bool,
) (kerr error) {

	meth := "TCPMappingTools.ExecuteActionInMap()"

	if tcpmapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "TCPMappings is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range tcpmapping {
		var err error = nil
		switch action {
		case common.ActionCreate:
			err = t.Create(v, primaryObject)
		case common.ActionUpdate:
			err = t.Update(v, primaryObject)
		case common.ActionDelete:
			err = t.Delete(v, primaryObject)
		default:
			msg := fmt.Sprintf("Invalid action '%s'", action)
			log.Warnf("%s %s", meth, msg)
			err = kerrors.NewKukuError(kerrors.Retry, msg)
			return
		}
		abort, errToReturn := kerrors.CheckErrorEx(err, pendingRetryErrors)
		if abort {
			return errToReturn
		}
	}

	return
}

// Create adds the tcpmapping
func (t TCPMappingTools) Create(
	tcpmapping *ambv3alpha1.TCPMapping,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "TCPMappingTools.Create()"

	if tcpmapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "TCPMapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := tcpmapping.GetName()
	ns := tcpmapping.GetNamespace()
	log.Infof("%s tcpmapping %s to be added", meth, name)

	found, err := t.Exist(tcpmapping)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	} else if found {
		err = nil
		log.Infof("%s TCPMapping %s already exists", meth, name)
		return
	}

	ctx := context.TODO()
	options := metav1.CreateOptions{
		FieldManager: "kumori-kuinbound",
	}
	tcpmapping, err = t.clientset.GetambassadorV3alpha1().TCPMappings(ns).Create(ctx, tcpmapping, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceCreated, "TCPMapping", tcpmapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceCreated, msg)
	log.Infof("%s TCPMapping %s created", meth, name)
	return
}

// Update updates the tcpmapping
func (t TCPMappingTools) Update(
	tcpmapping *ambv3alpha1.TCPMapping,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "TCPMappingTools.Update()"

	if tcpmapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "TCPMapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := tcpmapping.GetName()
	ns := tcpmapping.GetNamespace()
	log.Infof("%s tcpmapping %s to be updated", meth, name)

	ctx := context.TODO()
	options := metav1.UpdateOptions{
		FieldManager: "kumori-kuinbound",
	}
	tcpmapping, err := t.clientset.GetambassadorV3alpha1().TCPMappings(ns).Update(ctx, tcpmapping, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceUpdated, "TCPMapping", tcpmapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceUpdated, msg)
	log.Infof("%s TCPMapping %s updated", meth, name)

	return
}

// Delete removes the tcpmapping
func (t TCPMappingTools) Delete(
	tcpmapping *ambv3alpha1.TCPMapping,
	primaryObject runtime.Object,
) (kerr error) {

	meth := "TCPMappingTools.Delete()"

	if tcpmapping == nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, "TCPMapping is nil")
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	name := tcpmapping.GetName()
	ns := tcpmapping.GetNamespace()
	log.Infof("%s tcpmapping %s to be removed", meth, name)

	policy := metav1.DeletePropagationForeground
	options := metav1.DeleteOptions{
		PropagationPolicy: &policy,
	}
	ctx := context.TODO()
	err := t.clientset.GetambassadorV3alpha1().TCPMappings(ns).Delete(ctx, name, options)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	msg := fmt.Sprintf(common.MessageResourceDeleted, "TCPMapping", tcpmapping.GetName())
	t.recorder.Event(primaryObject, corev1.EventTypeNormal, common.ReasonResourceDeleted, msg)
	log.Infof("%s TCPMapping %s removed", meth, name)

	return
}

// Exists checks if the tcpmapping already exists
func (t TCPMappingTools) Exist(tcpmapping *ambv3alpha1.TCPMapping) (bool, error) {
	meth := "TCPMappingTools.Exist()"
	if tcpmapping == nil {
		return false, nil
	}
	name := tcpmapping.GetName()
	ns := tcpmapping.GetNamespace()
	foundTcpMapping, err := t.lister.TCPMappings(ns).Get(name)
	if errors.IsNotFound(err) || foundTcpMapping == nil {
		return false, nil
	}
	if err != nil {
		log.Errorf("%s %s", meth, err.Error())
		return false, err
	}
	if foundTcpMapping != nil {
		return true, nil
	}
	return false, nil
}

// GetCurrentMap returns the map of tcpmapping currently related to
// an owner object (tipically, a kukuhttpinbound object): searchs using the
// owner label and checks ownerRef value
func (t TCPMappingTools) GetCurrentMap(
	owner metav1.Object,
) (
	currentTCPMappings map[string]*ambv3alpha1.TCPMapping, kerr error,
) {

	meth := "TCPMappingTools.GetCurrentMap()"
	currentTCPMappings = make(map[string]*ambv3alpha1.TCPMapping)

	name := owner.GetName()
	ns := owner.GetNamespace()
	matchLabels := map[string]string{common.OwnerLabel: name}
	labelSelector := labels.SelectorFromSet(matchLabels)

	list, err := t.lister.TCPMappings(ns).List(labelSelector)
	if err != nil {
		kerr = kerrors.NewKukuError(kerrors.Retry, err)
		log.Errorf("%s %s", meth, kerr.Error())
		return
	}

	for _, v := range list {
		if metav1.IsControlledBy(v, owner) {
			currentTCPMappings[v.GetName()] = v
		} else {
			// This tcpmapping hasn't the provided object as OwnerRef!
			msg := "TCPMapping " + v.GetName() + " is not controlled by " + owner.GetName()
			kerr = kerrors.NewKukuError(kerrors.Retry, msg)
			log.Errorf("%s %s", meth, kerr.Error())
			return
		}
	}

	return
}

// MergeToCurrent copy properties of newobj into currentobj
func (t TCPMappingTools) MergeToCurrent(
	currentobj *ambv3alpha1.TCPMapping, newobj *ambv3alpha1.TCPMapping,
) *ambv3alpha1.TCPMapping {
	currentobj.Spec = *newobj.Spec.DeepCopy()
	return currentobj
}

// Equal checks if two tcpmapping are equivalent. Just spec is compared.
func (t TCPMappingTools) Equal(
	o1, o2 *ambv3alpha1.TCPMapping,
) bool {
	if o1 == nil && o2 == nil {
		return true
	}
	if o1 == nil && o2 != nil {
		return false
	}
	if o1 != nil && o2 == nil {
		return false
	}
	return apiequality.Semantic.DeepEqual(o1.Spec, o2.Spec)
}

// GenerateLinkedTCPMapping creates a new tcpmapping related to kukulink.
//
// Example of generated object:
//
//		apiVersion: getambassador.io/v3alpha1
//		kind:  TCPMapping
//		metadata:
//		  name: kl-8e42cbe9e00014eed16021eaa0432ae192af6989-tcpmapping-linked
//		  namespace: kumori
//		  labels:
//		    owner: kl-8e42cbe9e00014eed16021eaa0432ae192af6989
//		  ownerReferences: [...]
//		spec:
//		  ambassadorID
//		  - --apiVersion-v3alpha1-only--default  (setted just as "default" in code)
//		  port: 1234
//		  service: serviceconnector000-0-kd-191558-733e723f-lb.kumori:80
//		  cluster_tag: dbe794b7
//		  resolver: kumori-default, or nothing if kubernetes-service balancer is
//	             used
func (t TCPMappingTools) GenerateLinkedTCPMapping(
	tcpInboundConfig *inboundconfig.TcpInboundConfig,
	kukulink *kumoriv1.KukuLink,
	kubeServiceName string,
	kubeServicePort int,
) (
	mapping *ambv3alpha1.TCPMapping,
) {
	meth := "TCPMappingTools.GenerateLinkedTCPMapping()"
	ownerNamespace := kukulink.GetNamespace()
	ownerName := kukulink.GetName()
	port := tcpInboundConfig.Port.Spec.Port
	tcpMappingName := naming.CalculateLinkTCPMappingName(ownerName, port)
	log.Infof("%s %s", meth, tcpMappingName)

	labels := make(map[string]string)
	labels[common.OwnerLabel] = ownerName

	meta := metav1.ObjectMeta{
		Name:      tcpMappingName,
		Namespace: ownerNamespace,
		OwnerReferences: []metav1.OwnerReference{
			*metav1.NewControllerRef(kukulink, schema.GroupVersionKind{
				Group:   kumoriv1.SchemeGroupVersion.Group,
				Version: kumoriv1.SchemeGroupVersion.Version,
				Kind:    common.KindKukuLink,
			}),
		},
	}
	meta.SetLabels(labels)

	mappingService := kubeServiceName + "." + ownerNamespace + ":" + strconv.Itoa(kubeServicePort)

	// Generates the TCPMapping spec
	//
	// About clusterTag: this is the crc of the a string that contains the ID of
	// the v3dep and the kube-service.
	// Why use "ClusterTag"?
	// - https://gitlab.com/kumori/kv3/project-management/-/issues/862
	// - https://github.com/emissary-ingress/emissary/issues/3112
	//   (it is not clear if using Ambassador v3 is still an error, but the
	//   clustertag is fine in any case)
	//
	// About resolver:
	// The definition of TCPMapping allows setting the attribute "resolver", and we
	// would like to set a "kubernetesendpointresolver".
	// - Using Ambassador v1.x, the resolver is ignored, and it is always setted
	//   "kubernetesserviceresolver". That is: Ambassador/Envoy will not perform
	//   any balancing, which is delegated to the "kube-service".
	// - Using Ambassador v3.x, the resolved can be setted, and it is the
	//   preferred option... but I dont see that the TCPMapping includes a
	//   loadbalancer field!
	longClusterTag := tcpInboundConfig.ID + "/" + mappingService
	clusterTag := helper.CrcString(longClusterTag)
	spec := ambv3alpha1.TCPMappingSpec{
		AmbassadorID: common.AmbassadorID(),
		Port:         port,
		Service:      mappingService,
		ClusterTag:   clusterTag,
	}

	mapping = &ambv3alpha1.TCPMapping{
		ObjectMeta: meta,
		Spec:       spec,
	}

	return
}
