/*
* Copyright 2022 Kumori systems S.L.
*
* Licensed under the EUPL, Version 1.2 or – as soon they
* will be approved by the European Commission - subsequent
* versions of the EUPL (the "Licence");
* You may not use this work except in compliance with the
* Licence.
* You may obtain a copy of the Licence at:
*
* https://joinup.ec.europa.eu/software/page/eupl
*
* Unless required by applicable law or agreed to in
* writing, software distributed under the Licence is
* distributed on an "AS IS" basis,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
* express or implied.
* See the Licence for the specific language governing
* permissions and limitations under the Licence.
 */

package main

import (
	"flag"
	"fmt"
	"os"
	"os/user"
	"path/filepath"
	"time"

	cacontroller "httpinbound-controller/pkg/controllers/kukuca"
	certcontroller "httpinbound-controller/pkg/controllers/kukucert"
	linkcontroller "httpinbound-controller/pkg/controllers/kukulink"
	v3depcontroller "httpinbound-controller/pkg/controllers/v3deployment"
	common "httpinbound-controller/pkg/utils/common"
	helper "httpinbound-controller/pkg/utils/helper"

	stopsignal "gitlab.com/kumori-systems/community/libraries/base-controller/pkg/stop-signal"
	kukuclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/clientset/versioned"
	kukuinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated/informers/externalversions"

	ambclientset "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/clientset/versioned"
	ambinformers "gitlab.com/kumori-systems/community/libraries/client-go/pkg/generated_ambassador/informers/externalversions"

	fsnotify "github.com/fsnotify/fsnotify"
	log "github.com/sirupsen/logrus"
	pflag "github.com/spf13/pflag"
	viper "github.com/spf13/viper"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	kubeinformers "k8s.io/client-go/informers"
	kubeclientset "k8s.io/client-go/kubernetes"
	rest "k8s.io/client-go/rest"
	clientcmd "k8s.io/client-go/tools/clientcmd"
)

// Ugly! We would prefer an automatic version numbering
var controllerVersion string

// How many workers will be launched
const threadiness int = 1

// Command-line args (see init function)
var version bool
var kubeconfig string
var resyncInterval time.Duration

// init reads command-line args and configmap configuration
// (init function is executed when package is imported)
func init() {
	meth := "main.init"

	// Initialize logs (for now, debug level -- changed later). By default, log
	// use the stdout writter
	log.SetFormatter(&log.TextFormatter{FullTimestamp: true})
	log.SetLevel(log.DebugLevel)

	controllerVersion = "HttpInbound Controller v3.1.1-experimental-task1734"

	// Special flag: used to print controller version
	flag.BoolVar(&version, "version", false, "Prints current controller version")

	// Configuration can be provided via command-line flags or via YAML configuration
	// file.
	// Usually: command-line is used to execute the controller in a development
	// machines, and YAML file to execute the controller within a Kubernetes
	// cluster (injected via configmap)
	flag.String("logLevel", "debug", "Minimum severity to show a log line: debug, info (default), warn, err")
	flag.String("kubeconfig", "", "Paths to a kubeconfig. Only required if out-of-cluster.")
	flag.Duration("resync", 0, "Resync interval (5s, 1m...). Never, if not specified or 0.")
	flag.String("ingresscname", "", "CNAME for DNS records (provisional - ticket161).")
	flag.String("minTLSVersion", "v1.2", "Minimum supported TLS protocol version (default: v1.2).")
	flag.String("maxTLSVersion", "", "Maximum supported TLS protocol version")
	flag.String("cipherSuitesTLS", "", "Supported ciphers (comma-separated list of strings with no spaces) when negotiating a TLS 1.0-1.2 connection.")
	flag.String("ecdhCurvesTLS", "", "Supported ECDH curves (comma-separated list of strings with no spaces) when negotiating a TLS connection.")
	flag.String("inboundVersions", "kumori.systems/builtins/inbound/@1.0.0:service", "Supported builtin inbound services (comma-separated list of modules.")
	flag.Int("httpPort", 8080, "Internal HTTP port. Used for redirecting HTTP to HTTPS. Set to 0 to disable redirection.")

	// Viper will contain the mix of command-line flags and YAML configuration file
	// parameters. If YAML configuration file changes while KuInbound is runnings,
	// viper detects it and reload configuration (see viper.WatchConfig() in
	// controllers implementation)
	pflag.CommandLine.AddGoFlagSet(flag.CommandLine)
	pflag.Parse()
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("/kumori")
	viper.BindPFlags(pflag.CommandLine)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Warnf("%s. Config file '/kumori/config.yaml' not found. Using default/command-line values.", meth)
		} else {
			log.Errorf("%s. Error reading configuration file in '/kumori/config.yaml': %s", meth, err.Error())
		}
	}

	kubeconfig = viper.GetString("kubeconfig")
	resyncInterval = viper.GetDuration("resync")
	helper.UpdateLogLevel(viper.GetString("logLevel"))

	log.Infof("%s Configuration: %v", meth, viper.AllSettings())
}

// getConfig loads a REST Config, depending of the environment and flags:
// Config precedence:
// * --kubeconfig flag pointing at a file
// * KUBECONFIG environment variable pointing at a file
// * In-cluster config if running in cluster
// * $HOME/.kube/config if exists (default location in the user's home directory)
func getConfig() (config *rest.Config, err error) {
	if len(kubeconfig) > 0 {
		return clientcmd.BuildConfigFromFlags("", kubeconfig)
	}
	if len(os.Getenv("KUBECONFIG")) > 0 {
		return clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
	}
	if c, err := rest.InClusterConfig(); err == nil {
		return c, nil
	}
	if usr, err := user.Current(); err == nil {
		c, err := clientcmd.BuildConfigFromFlags("", filepath.Join(usr.HomeDir, ".kube", "config"))
		if err == nil {
			return c, nil
		}
	}
	return nil, fmt.Errorf("could not locate a kubeconfig")
}

// setQPSBurst applies saner defaults for QPS and burst based on the Kubernetes
// controller manager defaults (20 QPS, 30 burst)
// These 2 flags set normal and burst rate that controller can talk to
// kube-apiserver.
// Default values work pretty well (20 for QPS and 30 for burst).
// Increase these values for large, production clusters.
func setQPSBurst(config *rest.Config) {
	if config.QPS == 0.0 {
		config.QPS = 20.0
		config.Burst = 30.0
	}
}

// main is the entrypoint of the controller
func main() {
	meth := "main.main()"

	// Set up signals so we handle the shutdown signal gracefully
	stopCh := stopsignal.SetupStopSignal()

	flag.Parse()
	if version {
		fmt.Println(controllerVersion)
		return
	}

	log.Infof("%s Running httpinboundcontroller...", meth)
	config, err := getConfig()
	if err != nil {
		log.Fatal("main.main() Error getting Kubernetes config: ", err)
	}
	setQPSBurst(config)

	// Create a clientset and informer-factory for kubernetes standard objects
	kubeClientset, err := kubeclientset.NewForConfig(config)
	if err != nil {
		log.Fatalf("%s Error building kubernetes clientset: %v", meth, err)
	}
	kubeInformerFactory := kubeinformers.NewFilteredSharedInformerFactory(
		kubeClientset,
		resyncInterval,
		"",
		func(opt *metav1.ListOptions) {},
	)

	// Create a clientset and informer-factory for Kuku objects
	kukuClientset, err := kukuclientset.NewForConfig(config)
	if err != nil {
		log.Fatalf("%s Error creating the kuku clientset: %v", meth, err)
	}
	kukuInformerFactory := kukuinformers.NewFilteredSharedInformerFactory(
		kukuClientset,
		resyncInterval,
		"",
		func(opt *metav1.ListOptions) {},
	)

	// Create a clientset and informer-factory for Amb objects
	ambClientset, err := ambclientset.NewForConfig(config)
	if err != nil {
		log.Fatalf("%s Error creating the amb clientset: %v", meth, err)
	}
	ambInformerFactory := ambinformers.NewFilteredSharedInformerFactory(
		ambClientset,
		resyncInterval,
		"",
		func(opt *metav1.ListOptions) {},
	)

	controllerConfig := common.NewControllerConfig(
		viper.GetString("ingresscname"),
		viper.GetString("minTLSVersion"),
		viper.GetString("maxTLSVersion"),
		viper.GetString("cipherSuitesTLS"),
		viper.GetString("ecdhCurvesTLS"),
		viper.GetInt("httpPort"),
		viper.GetString("inboundVersions"),
	)

	// Create the controllers
	caController := cacontroller.NewController(
		kubeClientset,
		kukuInformerFactory.Kumori().V1().KukuCas(), // Primary
		kubeInformerFactory.Core().V1().Secrets(),   // Secondary
	)
	certController := certcontroller.NewController(
		kubeClientset,
		kukuInformerFactory.Kumori().V1().KukuCerts(), // Primary
		kubeInformerFactory.Core().V1().Secrets(),     // Secondary
		controllerConfig,
	)
	v3depcontroller := v3depcontroller.NewController(
		kubeClientset,
		ambClientset,
		kukuInformerFactory.Kumori().V1().V3Deployments(),        // Primary
		kubeInformerFactory.Networking().V1().Ingresses(),        // Secondary
		ambInformerFactory.Getambassador().V3alpha1().Hosts(),    // Secondary
		ambInformerFactory.Getambassador().V3alpha1().Mappings(), // Secondary
		kukuInformerFactory.Kumori().V1().KukuDomains(),          // Special secondary
		kukuInformerFactory.Kumori().V1().KukuCerts(),            // Special secondary
		kukuInformerFactory.Kumori().V1().KukuCas(),              // Special secondary
		kukuInformerFactory.Kumori().V1().KukuPorts(),            // Special secondary
		kukuInformerFactory.Kumori().V1().KukuLinks(),            // Terciary
		controllerConfig,
	)
	linkController := linkcontroller.NewController(
		kubeClientset,
		ambClientset,
		kukuInformerFactory.Kumori().V1().KukuLinks(),               // Primary
		ambInformerFactory.Getambassador().V3alpha1().Mappings(),    // Secondary
		ambInformerFactory.Getambassador().V3alpha1().TCPMappings(), // Secondary
		ambInformerFactory.Getambassador().V3alpha1().Hosts(),       // Secondary
		kukuInformerFactory.Kumori().V1().V3Deployments(),           // Secondary
		kukuInformerFactory.Kumori().V1().KukuDomains(),             // Special secondary
		kukuInformerFactory.Kumori().V1().KukuPorts(),               // Special secondary
		kukuInformerFactory.Kumori().V1().KukuCerts(),               // Terciary
		kukuInformerFactory.Kumori().V1().KukuCas(),                 // Terciary
		kubeInformerFactory.Core().V1().Services(),                  // Terciary
		controllerConfig,
	)

	// Watches for changes in configuration. If a change is detected, each controller
	// must reevaluate all its objects
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Infof("%s. Config file changed: %s", meth, e.Name)
		helper.UpdateLogLevel(viper.GetString("logLevel"))
		controllerConfig := common.NewControllerConfig(
			viper.GetString("ingresscname"),
			viper.GetString("minTLSVersion"),
			viper.GetString("maxTLSVersion"),
			viper.GetString("cipherSuitesTLS"),
			viper.GetString("ecdhCurvesTLS"),
			viper.GetInt("httpPort"),
			viper.GetString("inboundVersions"),
		)
		// caController.Reconfig() this controller has no configuration
		certController.Reconfig(controllerConfig)
		v3depcontroller.Reconfig(controllerConfig)
		linkController.Reconfig(controllerConfig)
	})

	// Start informers
	kubeInformerFactory.Start(stopCh)
	kukuInformerFactory.Start(stopCh)
	ambInformerFactory.Start(stopCh)

	// Start controllers
	errCh := make(chan error)
	go caController.Run(threadiness, stopCh, errCh)
	go certController.Run(threadiness, stopCh, errCh)
	go v3depcontroller.Run(threadiness, stopCh, errCh)
	go linkController.Run(threadiness, stopCh, errCh)

	select {
	case err := <-errCh: // Detect errors starting controllers
		fmt.Println("main.main() Error running controller: ", err)
	case <-stopCh: // Wait application closed
		log.Infof("%s Closing httpinboundcontroller", meth)
	}
}
