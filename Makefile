# Get the currently used golang install path (in GOPATH/bin, unless GOBIN is set)
ifeq (,$(shell go env GOBIN))
GOBIN=$(shell go env GOPATH)/bin
else
GOBIN=$(shell go env GOBIN)
endif

# Run go fmt against code
fmt:
	go fmt ./...

# Run go vet against code
# There are some false-positives emitted when KuInbound code is analyzed, related
# with some Ambassador types (see comments in mapping.go/GenerateUnlinkedMapping)
vet:
	go vet ./...

# Build manager binary
# Note that "vet" is not executed because there are known warnings
manager: fmt
	go build -o bin/manager cmd/controller-manager/main.go

# Run against the configured Kubernetes cluster in ~/.kube/config
INGRESSCNAME ?= ingress-mycluster.mydomain.com
MINTLSVERSION ?= v1.2
INBOUNDVERSIONS ?= "kumori.systems/builtins/inbound/inbound/@1.0.0:service,kumori.systems/inbound/@1.0.0:service,kumori.systems/builtins/inbound/@1.0.1:service"
#INBOUNDVERSIONS ?= "kumori.systems/builtins/inbound/inbound/@1.0.1:service"
CIPHERSSUITETLS ?= "ECDHE-ECDSA-AES256-GCM-SHA384,ECDHE-ECDSA-AES128-GCM-SHA256,ECDHE-ECDSA-CHACHA20-POLY1305"
#CIPHERSSUITETLS ?= ""

run: manager
	bin/manager --resync=5m --ingresscname=${INGRESSCNAME} --minTLSVersion=${MINTLSVERSION} --cipherSuitesTLS=${CIPHERSSUITETLS} --inboundVersions=${INBOUNDVERSIONS}

# Only when a /kumori/config.yaml exists, so that configuration is used
run-with-config: manager
	bin/manager --resync=5m


############################
# DEVELOPMENT ONLY TARGETS #
############################

VERSION ?= 0.0.999
IMG ?= docker.io/kumoridev/kuinbound:${VERSION}

# Build the docker image
#
# IMPORTANT: check Dockerfile first (some actions required before)
#
docker-build:
	docker build . -t ${IMG}

# Push the docker image
docker-push:
	docker login
	docker push ${IMG}
	docker logout

# Push the docker image to a development cluster
docker-push-dev:
	kind load docker-image $IMG $IMG --name devtesting
